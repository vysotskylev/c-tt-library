symboliclegendre[n_, x_] := Solve[LegendreP[n, x] == 0];
legendreprime[n_, a_] := D[LegendreP[n, x], x] /. x -> a;
weights[n_, x_] := 2/((1 - x^2) legendreprime[n, x]^2);

(*how many terms should be generated*)
h = 20;

(* what numerical precision is desired? *)
precision = 16;

str = OpenWrite["lgvalues.txt"];
Write[str, "abcissae"];
Do[Write[str]; Write[str, n]; Write[str];
  nlist = symboliclegendre[n, x];
  xnlist = x /. nlist;
  Do[Write[str, Re[N[Part[xnlist, i], precision]]], {i, Length[xnlist]}];, {n, 2, h}];
Write[str, "weights"];
Do[Write[str]; Write[str, n]; Write[str];
  slist := symboliclegendre[n, x];
  xslist = x /. slist;
  Do[Write[str, Re[N[weights[n, Part[xslist, i]], precision]]], {i, Length[xslist]}];, {n, 2, h}];
Close[str];
