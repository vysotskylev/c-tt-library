# C++ TT library

This is library for integration of highly multivariate functions with singularities. It uses tensor train decomposition to appoximate the tensor of function values on a grid and then implicitly computes the integral estimate by a high-dimensional cubature rule.

For more details refer to the [paper](https://arxiv.org/abs/2103.12129).

## Setup and build

Install [OpenBLAS](https://github.com/xianyi/OpenBLAS/wiki/Installation-Guide) and Lapacke. For Ubuntu `apt` can be used:
```
sudo apt install liblapacke-dev libopenblas-dev
```

Then run `make`. The static library will be in `build/libtt.a` and the test binary in `build/run_tests`.

## Usage

### Tensor class

`TTensor<T>` is an abstract class (class template) representing a $d$-dimensional array (called here "tensor") with elements of type `T`. The essential idea is that we never want to store the entire tensors in memory (think 300-dimensional tensors). Thus, we represent a tensor as a "black-box" function $t(i_1, \dots, i_d)$ mapping a tuple of indices to a number of type `T` (either real or complex):
$$
    t: \{0,\dots,n_1-1\}\times\dots\times\{0,\dots,n_d-1\} \to \mathtt{T}
$$

To define your own tensor, inherit from `TTensor<T>` and override the `ComputeAt` method, e.g.
```
class TZeroTensor : public TTensor<float> {
public:
    TZeroTensor(std::vector<int> dims)
        : TTensor(dims)
    { }

    void ComputeAt(int count, const int* indices, Value* result) const override {
        for (int i = 0; i < count; ++i) {
            result[i] = 0.;
        }
    }
}
```
`ComputeAt` is passed an array `indices` of `count` multi-indices (i.e. tuples of indices) and must write `count` values 
```
t(indices[d*0        ],..., indices[d*1    -1]),
...,
t(indices[d*(count-1)],..., indices[d*count-1])
```
into the `result` array.

More interesting examples can be found in [./tests/test_arithmetics.cpp](./tests/test_arithmetics.cpp), e.g. `TExpTensor`, which represents the tensor with elements
$$
t(i_1, \dots, i_d) = \exp(0.01(i_1+\dots+i_d))
$$
in the case of `T = float`.

### Tensor train class

`TTensorTrain<T>` is a specific `TTensor<T>` that is represented in Tensor Train format, i.e. an element is computed as
$$
t(i_1, \dots, i_d) = G_1(i_1)\dots G_d(i_d),
$$
where $G_k(i_k)$ is a matrix of size $r_{k-1}\times r_k$ and the numbers $r_1, \dots, r_{d-1}$ are called the TT-ranks ($r_0$ and $r_d$ are assumed to be equal to 1).

A `TTensorTrain<T>` can be constructed from a black-box `TTensor<T>` using the static `TTensorTrain<T>::Approximate` method, e.g.
```
auto zero = TZeroTensor({2, 2, 2, 2});
auto tt = TTensorTrain<float>::Approximate(&zero, TTensorTrainParameters{});
```

`TTensorTrain<T>` can be added, subtracted and multiplied by scalar. The $L_2$ norm and dot product can also be computed.

### TT integration

A black box function can be integrated in a $d$-dimensional box using the Tensor Train format. The user just needs to call `TTIntegrate` function from [./tt/integrate/integrate.h](./tt/integrate/integrate.h).

```
template <typename TArg, typename TRes, typename Fun>
TTIntegrationResult<TRes> TTIntegrate(
    Fun f,
    const TBox& box,
    const TTIntegratorConfig& config);
```

The parameters are as follows:
  1. `Fun f`: a functor object that can be called with the following signature: `f(int count, TArg* points, TValue* result)`. The function must compute the integrand in `count` points whose coordinates are passed in `points` array and write the results to the `result` array.
  1. `TBox box`: a struct representing a $d$-dimensional box over which the integration will be done. `TBox::Min` and `TBox::Max` contain the coordinates of the "bottom left" and "upper right" corners correspondingly.
  1. `TTIntegratorConfig config`: struct containing settings governing the TT approximation and integration process. Maybe be left default.

The returned structure contains the appoximate integral value and (possibly) the error estimate.

Example of integrating $\sin(x_1 + \dots + x_d)$ over the cube $[0,1]^d$:
```
auto dim = 100;
auto fun = [dim] (int count, double* points, double* result) {
    for (int point_idx = 0; point_idx < count; ++point_idx) {
        auto s = 0.;
        for (int k = 0; k < dim; ++k) {
            s += points[dim * point_idx + k];
        }
        result[point_idx] = std::sin(s);
    }
};
auto box = TBox{
    vector<double>(dim, 0.0),
    vector<double>(dim, 1.0)
};
auto res = TTIntegrate(fun, box, TTIntegrationConfig{});
std::cout << "Integral is " << res.value << std::endl;
```

For more examples please refer to [./tests/test_integration.cpp](./tests/test_integration.cpp).
