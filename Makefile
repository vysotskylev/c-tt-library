.SUFFIXES: .cpp .o

CC = gcc
CXX = g++

CXXFLAGS = -I$(CURDIR) -MP -MMD -m64 -std=c++17 -Wall -Werror -fPIC
LINK_OPT = -lm -ldl -lopenblas -llapacke -lpthread 

ifdef ASAN
    CXXFLAGS += -fsanitize=address
    LINK_OPT += -fsanitize=address
endif

ifdef DEBUG
    CXXFLAGS += -g
else
    CXXFLAGS += -O3
endif

ifeq ($(SINGLE_THREAD), 1)
    $(info WARNING! Building single-threaded version of TT library)
else
    $(info Building multi-threaded version of TT library)
endif

TEST_COPT = $(CXXFLAGS)

DEPS = tt/core/lapack_wrap.cpp \
       tt/core/cross.cpp \
	   tt/core/matrix.cpp \
	   tt/core/simple_cross.cpp \
       tt/core/tensor.cpp \
	   tt/core/tensor_train.cpp \
	   tt/core/utils.cpp \
	   tt/integrate/config.cpp \
	   tt/integrate/integrate.cpp \
	   tt/integrate/quadrature.cpp

TESTS = $(wildcard tests/test_*.cpp) tests/main.cpp

.PHONY: all

all: lib tests

OBJS1 = $(DEPS:.cpp=.o)
OBJS = $(addprefix build/, $(OBJS1))
TEST_OBJS1 = $(TESTS:.cpp=.o)
TEST_OBJS = $(addprefix build/, $(TEST_OBJS1))

$(OBJS): build/%.o: %.cpp
	mkdir -p $(dir $(@))
	$(CXX) -c $(CXXFLAGS) $< $(LIBS) -o $@

$(TEST_OBJS): build/%.o: %.cpp
	mkdir -p $(dir $(@))
	$(CXX) -c -I$(CURDIR) $(TEST_COPT) $< $(LIBS) -o $@

lib: $(OBJS)
	ar rc build/libtt.a $(OBJS)
	ranlib build/libtt.a

tests: $(TEST_OBJS) lib
	$(CXX) $(TEST_COPT) $(TEST_OBJS) build/libtt.a $(LINK_OPT) -o build/run_tests

install_integrator: lib
	mkdir -p $(PREFIX)include/tt/integrate
	cp -r tt/integrate/config.h tt/integrate/integrate_interface.h $(PREFIX)include/tt/integrate
	install build/libtt.a $(PREFIX)lib

clean:
	rm -rf build/*

clean_contrib:
	rm -rf $(OPENBLAS)/install
	$(MAKE) -C $(OPENBLAS) clean

-include $(addprefix build/, $(DEPS:.cpp=.d))
-include $(addprefix build/, $(TESTS:.cpp=.d))
