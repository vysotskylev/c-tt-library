#include "helpers.h"

#include <tt/core/tensor.h>
#include <tt/core/tensor_train.h>

#include <contrib/catch2/catch.hpp>

using namespace std::complex_literals;

TEST_CASE("tt_basic")
{
    auto t1 = TTensorTrain<double>::MakeRankOne({{1,1}, {2,2,2}, {3,3,3}});
    REQUIRE(std::fabs(t1.Dot(t1) - 36 * 18) < 1e-8);

    auto t2 = TTensorTrain<std::complex<double>>::MakeRankOne({{1i,1i}, {2,2,2}, {3,3,3}});
    REQUIRE(std::fabs(t1.Dot(t1) - 36 * 18) < 1e-8);
}

template <typename T>
void TestExponentOfSum()
{
    class TExpTensor
        : public TTensor<T>
    {
    public:
        using TTensor<T>::TTensor;

        void ComputeAt(int count, const int* indices, T* result) const override
        {
            auto dim = this->get_dimensionality();
            for (auto point_idx = 0; point_idx < count; ++point_idx) {
                double s = 0;
                for (int i = 0; i < dim; i++) {
                    s += indices[point_idx * dim + i];
                }
                if constexpr (std::is_same_v<T, double>) {
                    result[point_idx] = exp(-0.01 * s);
                } else {
                    static_assert(std::is_same_v<T, std::complex<double>>);
                    result[point_idx] = exp(0.01i * s);
                }
            }
        }
    };

    int N = 10;
    int d = 10;

    TExpTensor tensor(std::vector<int>(d, N));

    TTensorTrainParameters parameters;
    parameters.tolerance = 1e-6;
    parameters.maximal_iterations_number = 0;

    auto tt = TTensorTrain<T>::Approximate(&tensor, parameters);

    for (int i = 0; i < d; i++) {
        REQUIRE(tt.GetRank(i) == 1);
    }

    std::vector<int> indices(d, 1);
    T res;
    tt.ComputeAt(1, indices.data(), &res);
    T expected;
    if constexpr (std::is_same_v<T, double>) {
        expected = exp(-0.01 * d);
    } else {
        expected = exp(0.01i * double(d));
    }
    REQUIRE(std::fabs(res - expected) < 1e-6);
}

// Tensor trains correctly approximates a(j1,...,jd) = e^(-0.01*(j1 + ... + jd))".
TEST_CASE("tt_exponent_of_sum_real")
{
    TestExponentOfSum<double>();
}

// Tensor trains correctly approximates a(j1,...,jd) = e^(0.01*i*(j1 + ... + jd))".
TEST_CASE("tt_exponent_of_sum_complex")
{
    TestExponentOfSum<std::complex<double>>();
}

// Tensor trains correctly approximates a(i1,...,id) = 0.01*(i1 + ... + id).
TEST_CASE("tt_sum")
{
    class TSumTensor
        : public TTensor<double>
    {
    public:
        using TTensor::TTensor;

        void ComputeAt(int count, const int* indices, double* result) const override
        {
            auto dim = this->get_dimensionality();
            for (auto point_idx = 0; point_idx < count; ++point_idx) {
                double s = 0;
                for (int i = 0; i < dim; i++) {
                    s += indices[point_idx * dim + i] * 1e-2;
                }
                result[point_idx] = s;
            }
        }
    };

    int N = 10;
    int d = 10;

    TSumTensor tensor(std::vector<int>(d, N));

    TTensorTrainParameters parameters;
    parameters.tolerance = 1e-6;
    parameters.maximal_iterations_number = 0;

    auto tt = TTensorTrain<double>::Approximate(&tensor, parameters);

    REQUIRE(tt.GetRank(0) == 1);
    for (int i = 1; i < d; i++) {
        REQUIRE(tt.GetRank(i) == 2);
    }

    std::vector<int> indices(d, 1);
    double res;
    tt.ComputeAt(1, indices.data(), &res);
    auto expected = 0.01 * d;
    REQUIRE(std::fabs(res - expected) < 1e-6);
}


// Tensor trains approximation respects evaluation limits.
TEST_CASE("tt_max_evaluation_count")
{
    int N = 2;
    int d = 10;

    TRandomTensor<double> tensor(std::vector<int>(d, N));

    TTensorTrainParameters parameters;
    parameters.tolerance = 1e-6;
    parameters.maximal_iterations_number = 0;

    static const int64_t kMaxEvaluationCount = 1000;
    parameters.evaluation_count_hard_limit = kMaxEvaluationCount;
    parameters.evaluation_count_soft_limit = kMaxEvaluationCount;

    auto tt = TTensorTrain<double>::Approximate(&tensor, parameters);

    REQUIRE(tensor.GetEvals() < kMaxEvaluationCount + 500);
}
