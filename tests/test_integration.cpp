#include "helpers.h"

#include <tt/integrate/integrate.h>

#include <contrib/catch2/catch.hpp>

#include <iostream>
#include <vector>
#include <complex>
#include <numeric>
#include <cmath>

using namespace std;
using namespace std::complex_literals;

template <typename TFun, typename T>
void TestIntegration(int dim, TFun fun, T expected)
{
    vector<int> step_counts(dim, 40);

    auto batch_fun = [dim, fun] (int count, auto* points, auto* result) {
        for (auto point_idx = 0; point_idx < count; ++point_idx) {
            auto beg = points + point_idx * dim;
            auto end = points + (point_idx + 1) * dim;
            result[point_idx] = fun(beg, end);
        }
    };
    TBox box = {
        vector<double>(dim, 0.0),
        vector<double>(dim, 1.0)
    };

    SECTION("power_transform") {
        TTIntegratorConfig config;
        config.type = QT_GAUSS_MIXED;
        config.transform = TR_POWER;
        config.node_count = 40;
        config.approximation_tolerance = 1e-7;
        auto res = TTIntegrate<double, T>(batch_fun, box, config);
        REQUIRE(abs(expected - res.value) < 1e-7);
        REQUIRE(!res.error_estimate.has_value());
    }
    SECTION("tanh_sinh") {
        TTIntegratorConfig config;
        config.type = QT_TANH_SINH;
        config.node_count = 101;
        config.approximation_tolerance = 1e-7;
        auto res = TTIntegrate<double, T>(batch_fun, box, config);
        REQUIRE(abs(expected - res.value) < 1e-6);
        REQUIRE(!res.error_estimate.has_value());
    }
    SECTION("gauss_kronrod") {
        TTIntegratorConfig config;
        config.type = QT_GAUSS_KRONROD;
        config.transform = TR_POWER;
        config.node_count = 19;
        config.simple_node_count = 19;
        config.approximation_tolerance = 1e-7;
        auto res = TTIntegrate<double, T>(batch_fun, box, config);
        REQUIRE(abs(expected - res.value) < 1e-6);
        REQUIRE(res.error_estimate.has_value());
        REQUIRE(std::abs(*res.error_estimate) < 1e-5);
    }
}

// Tensor trains integration works for sin(x1 + ... + xN).
TEST_CASE("integrate_sin_of_sum")
{
    int dim = 10;
    auto expected = pow(((exp(1i) - 1.0) / 1i), dim).imag();
    auto fun = [] (double* beg, double* end) {
        return sin(std::accumulate(beg, end, 0.0));
    };
    TestIntegration(dim, fun, expected);
}

// Tensor trains integration works for exp(i*(x1 + ... + xN)).
TEST_CASE("integrate_exp_of_sum")
{
    int dim = 10;
    auto expected = pow(((exp(1i) - 1.0) / 1i), dim);
    auto fun = [] (double* beg, double* end) {
        return exp(1i * std::accumulate(beg, end, 0.0));
    };
    TestIntegration(dim, fun, expected);
}

// Tensor trains integration works for i*(x1 + ... + xN).
TEST_CASE("integrate_sum")
{
    int dim = 10;
    auto expected = 0.5i * double(dim);
    auto fun = [] (double* beg, double* end) {
        return 1i * std::accumulate(beg, end, 0.0);
    };
    TestIntegration(dim, fun, expected);
}

// Tensor trains integration respects evaluation limits.
TEST_CASE("integrate_evaluation_count_limits")
{
    static const int64_t kMaxEvaluationCount = 10000;
    int dim = 10;
    int N = 4;
    vector<int> step_counts(dim, N);

    TRandomTensor<double> tensor(std::vector<int>(dim, N));
    auto fun = [&] (int count, double* points, double* result) {
        std::vector<int> indices;
        for (auto point_idx = 0; point_idx < count; ++point_idx) {
            for (auto i = 0; i < dim; ++i) {
                auto coord = points[point_idx * dim + i];
                indices.push_back(int(coord * N + 0.1));
            }
        }
        return tensor.ComputeAt(count, indices.data(), result);
    };
    TBox box = {
        vector<double>(dim, 0.0),
        vector<double>(dim, 1.0)
    };

    TTIntegratorConfig config;
    config.type = QT_GAUSS_MIXED;
    config.transform = TR_POWER;
    config.node_count = 40;
    config.approximation_tolerance = 1e-7;
    config.evaluation_count_hard_limit = kMaxEvaluationCount;
    config.evaluation_count_soft_limit = kMaxEvaluationCount;
    TTIntegrate<double, double>(fun, box, config);

    REQUIRE(tensor.GetEvals() < 2 * kMaxEvaluationCount);
}

TEST_CASE("two_singularities")
{
    int dim = 3;

    TTIntegratorConfig config;
    config.type = QT_TANH_SINH;
    config.transform = TR_POWER;
    config.power_transform_power = 4;
    config.tanh_sinh_step = 0.1;
    config.node_count = 51;
    config.approximation_tolerance = 1e-7;

    TBox box = {
        vector<double>(dim, 0.0),
        vector<double>(dim, 1.0)
    };

    auto orig_fun = [] (double x, double y) {
        return y/sqrt(x*(1-x));
    };

    auto fun = [&] (int count, double* points, double* result) {
        for (auto point_idx = 0; point_idx < count; ++point_idx) {
            auto beg = points + point_idx * dim;
            // algebraic singularity
            const auto x = *beg;
            const auto y = *(beg+1);
            result[point_idx] = orig_fun(x, y);//0.5 * (orig_fun(x/2, y) + orig_fun(1-x/2, y));
        }
    };

    auto res = TTIntegrate<double, double>(fun, box, config);
    auto expected = 2 * atan(1.0);

    REQUIRE(abs(res.value - expected) < 1e-4);
}

TEST_CASE("two_singularities_1")
{
    int dim = 3;

    TBox box = {vector<double>(dim, 0.0), vector<double>(dim, 1.0)};

    auto orig_fun = [](double x, double y) { return y / sqrt(x * (1 - x)); };

    auto fun1 = [&](int count, double* points, double* result) {
        for (auto point_idx = 0; point_idx < count; ++point_idx) {
            auto beg = points + point_idx * dim;
            const auto x = *beg;
            const auto y = *(beg + 1);
            result[point_idx] = 0.5 * (orig_fun(x / 2, y) + orig_fun(1 - x / 2, y));
        }
    };

    auto fun2 = [&](int count, double* points, double* result) {
        for (auto point_idx = 0; point_idx < count; ++point_idx) {
            auto beg = points + point_idx * dim;
            const auto x = *beg;
            const auto y = *(beg + 1);
            result[point_idx] = orig_fun(x, y);
        }
    };

    const auto expected = 2 * atan(1.0);

    TTIntegratorConfig config1;
    config1.type = QT_GAUSS_MIXED;
    config1.transform = TR_POWER;
    config1.power_transform_power = 4;
    config1.approximation_tolerance = 1e-7;
    auto res1 = TTIntegrate<double, double>(fun1, box, config1);

    REQUIRE(abs(res1.value - expected) < 1e-7);

    TTIntegratorConfig config2;
    config2.type = QT_TANH_SINH;
    config2.tanh_sinh_step = 0.1;
    config2.node_count = 51;
    config2.approximation_tolerance = 1e-7;
    auto res2 = TTIntegrate<double, double>(fun2, box, config2);

    REQUIRE(abs(res2.value - expected) < 1e-4);
}
