#include <tt/core/tensor.h>
#include <tt/core/tensor_train.h>

#include <complex>
#include <random>
#include <unordered_map>

class TVectorHash
{
public:
    std::size_t operator()(const std::vector<int>& vec) const
    {
        std::size_t seed = vec.size();
        for (auto i : vec) {
            seed ^= static_cast<std::size_t>(i) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
        }
        return seed;
    }
};

template <typename T>
class TDistr : public std::uniform_real_distribution<T>
{ };

template <typename T>
class TDistr<std::complex<T>>
{
public:
    template <typename TRng>
    std::complex<T> operator() (TRng& rng)
    {
        return {re_(rng), im_(rng)};
    }

private:
    std::uniform_real_distribution<T> re_{0, 1};
    std::uniform_real_distribution<T> im_{0, 1};
};

template <typename T>
class TRandomTensor
    : public TTensor<T>
{
public:
    using TTensor<T>::TTensor;

    void ComputeAt(int count, const int* indices, T* result) const override
    {
        ++evals_;
        auto dim = this->get_dimensionality();
        for (auto point_idx = 0; point_idx < count; ++point_idx) {
            std::vector<int> p(indices + point_idx * dim, indices + (point_idx + 1) * dim);
            auto it = memo_.find(p); 
            if (it != memo_.end()) {
                result[point_idx] = it->second;
            } else {
                auto val = distr_(gen_);
                memo_.emplace(std::move(p), val);
                result[point_idx] = val;
            }
        }
    }

    int64_t GetEvals() const
    {
        return evals_;
    }

private:
    mutable int64_t evals_{0};
    mutable std::mt19937 gen_{42};
    mutable TDistr<T> distr_;
    mutable std::unordered_map<std::vector<int>, double, TVectorHash> memo_;
};
