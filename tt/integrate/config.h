#pragma once

#include <cstdint>
#include <limits>

enum EQuadratureType
{
    QT_TRAPEZOIDAL,
    QT_TANH_SINH,
    // Integrate functions with a singularity at lower left corner.
    QT_SINGULARITY_MIXED,
    QT_GAUSS_MIXED,
    QT_GAUSS_KRONROD,
};

enum ETransform
{
    TR_IDENTITY,
    // Apply transform f(x) -> f(x^p) before integration. Suitable for log-singularities.
    TR_POWER,
};

struct TTIntegratorConfig
{
    double approximation_tolerance = 1e-6;
    int approximation_iteration_count = 8;

    enum EQuadratureType type = QT_TRAPEZOIDAL;
    int node_count = 40;

    // For tanh-sinh.
    double tanh_sinh_step = 0.05;

    // For singularity_mixed.
    int singularity_node_count = 0;
    double singularity_width = 0;

    // Number of nodes in each simple quadrature.
    int simple_node_count = 4;

    enum ETransform transform = TR_IDENTITY;

    // For power_transform.
    int power_transform_power = 3;

    int64_t evaluation_count_soft_limit = std::numeric_limits<int64_t>::max();
    int64_t evaluation_count_hard_limit = std::numeric_limits<int64_t>::max();
};
