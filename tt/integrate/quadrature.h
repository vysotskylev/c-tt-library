#pragma once

#include "config.h"
#include "types.h"

#include <optional>
#include <vector>

struct TQuadrature
{
    std::vector<std::vector<double>> Abscissas, Weights;
    std::optional<std::vector<std::vector<double>>> WeightsForErrorEstimate;
};

TQuadrature CreateQuadrature(const TBox& box, const TTIntegratorConfig& config);
