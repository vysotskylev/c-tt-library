#include "quadrature.h"
#include "quadrature_db.h"

#include <cmath>
#include <stdexcept>
#include <cassert>

#include <iostream>
#include <sstream>
#include <functional>

static constexpr double pi = 3.141592653589793;

static TQuadrature CreateTrapezoidalQuadrature(double left, double right, const TTIntegratorConfig& config)
{
    std::vector<double> abscissas, weights;
    abscissas.reserve(config.node_count);
    weights.reserve(config.node_count);
    double step = (right - left) / (config.node_count - 1);
    for (int i = 0; i < config.node_count; ++i) {
        abscissas.push_back(left + i * step);
        if (i == 0 || i == config.node_count - 1) {
            weights.push_back(step / 2);
        } else {
            weights.push_back(step);
        }
    }

    TQuadrature q;
    q.Abscissas.assign(1, std::move(abscissas));
    q.Weights.assign(1, std::move(weights));
    return q; 
}

static void FillTanhSinh(
    double left,
    double right,
    int pointCount,
    double step,
    std::vector<double>* abscissas,
    std::vector<double>* weights)
{
    if (pointCount % 2 != 1) {
        throw std::logic_error("Number of points for tanh-sinh quadrature must be odd");
    }

    int maxIndex = pointCount / 2;
    for (int i = -maxIndex; i <= maxIndex; ++i) {
        auto x = std::tanh(pi / 2 * std::sinh(i * step));
        auto scaledX = left + (1 + x) / 2 * (right - left);
        abscissas->push_back(scaledX);
        auto tmp = std::cosh(pi / 2  * std::sinh(i * step));
        auto w = step * pi / 2 * std::cosh(i * step) / (tmp * tmp);
        auto scaledW = w * (right - left) / 2;
        weights->push_back(scaledW);
    }
}

static TQuadrature CreateTanhSinhQuadrature(double left, double right, const TTIntegratorConfig& config)
{
    std::vector<double> abscissas, weights;
    FillTanhSinh(left, right, config.node_count, config.tanh_sinh_step, &abscissas, &weights);
    TQuadrature q;
    q.Abscissas.assign(1, std::move(abscissas));
    q.Weights.assign(1, std::move(weights));
    return q; 
}

static void FillGauss(
    double left,
    double right,
    int pointCount,
    std::vector<double>* abscissas,
    std::vector<double>* weights)
{
    if (pointCount <= 0 || pointCount > quadratures::Gauss::kMaxPointCount) {
        throw std::logic_error("Point count for Gauss quadrature must be in [1, " + std::to_string(quadratures::Gauss::kMaxPointCount) + "]");
    }

    for (int i = 0; i < pointCount; ++i) {
        auto x = quadratures::Gauss::Abscissas[pointCount][i];
        auto scaledX = left + (x + 1) / 2 * (right - left);
        abscissas->push_back(scaledX);
        auto scaledW = quadratures::Gauss::Weights[pointCount][i] * (right - left) / 2;
        weights->push_back(scaledW);
    }
}

static TQuadrature CreateSingularityMixedQuadrature(double left, double right, const TTIntegratorConfig& config)
{
    std::vector<double> abscissas, weights;
    FillTanhSinh(
        left,
        left + (right - left) * config.singularity_width,
        config.singularity_node_count,
        config.tanh_sinh_step,
        &abscissas,
        &weights);
    auto intervalCount = (config.node_count - config.singularity_node_count) / config.simple_node_count;
    if (intervalCount <= 0) {
        std::stringstream ss;
        ss << "quadrature_singularity_node_count + quadrature_simple_node_count must not exceed quadrature_node_count, "
           << "got " << config.singularity_node_count << " + " << config.simple_node_count << " > " << config.node_count;
        throw std::logic_error(ss.str());
    }
    if ((config.node_count - config.singularity_node_count) % config.simple_node_count != 0) {
        std::stringstream ss;
        ss << "quadrature_node_count - quadrature_singularity_node_count must be divisible by quadrature_simple_node_count, "
           << "got " << config.node_count << " - " << config.singularity_node_count << " mod " << config.simple_node_count << " != 0";
        throw std::logic_error(ss.str());
    }

    auto newLeft = left + config.singularity_width * (right - left);
    auto intervalWidth = (right - newLeft) / intervalCount;
    for (int i = 0; i < intervalCount; ++i) {
        FillGauss(
            newLeft + intervalWidth * i,
            newLeft + intervalWidth * (i + 1),
            config.simple_node_count,
            &abscissas,
            &weights);
    }
     
    TQuadrature q;
    q.Abscissas.assign(1, std::move(abscissas));
    q.Weights.assign(1, std::move(weights));
    return q; 
}

static TQuadrature CreateCompositeGaussMixedQuadrature(double left, double right, const TTIntegratorConfig& config)
{
    std::vector<double> abscissas, weights;
    if (config.node_count % config.simple_node_count != 0) {
        std::stringstream ss;
        ss << "quadrature_node_count must be divisible by quadrature_simple_node_count, "
           << "got (" << config.node_count << " - " << config.singularity_node_count << ") mod " << config.simple_node_count << " != 0";
        throw std::logic_error(ss.str());
    }
    if (config.singularity_node_count % config.simple_node_count != 0) {
        std::stringstream ss;
        ss << "quadrature_singularity_node_count must be divisible by quadrature_simple_node_count, "
           << "got (" << config.singularity_node_count << " - " << config.singularity_node_count << ") mod " << config.simple_node_count << " != 0";
        throw std::logic_error(ss.str());
    }

    auto fillIntervals = [&abscissas, &weights] (double left, double right, int pointCount, int gaussPointCount) {
        auto intervalCount = pointCount / gaussPointCount;
        auto intervalWidth = (right - left) / intervalCount;
        for (int i = 0; i < intervalCount; ++i) {
            FillGauss(
                left + intervalWidth * i,
                left + intervalWidth * (i + 1),
                gaussPointCount,
                &abscissas,
                &weights);
        }
    };

    auto singularityBorder = left + (right - left) * config.singularity_width;
    fillIntervals(left, singularityBorder, config.singularity_node_count, config.simple_node_count);
    fillIntervals(singularityBorder, right, config.node_count - config.singularity_node_count, config.simple_node_count);

    TQuadrature q;
    q.Abscissas.assign(1, std::move(abscissas));
    q.Weights.assign(1, std::move(weights));
    return q; 
}

static void FillGaussKronrod(
    double left,
    double right,
    int pointCount,
    std::vector<double>* abscissas,
    std::vector<double>* weights,
    std::vector<double>* gaussWeights)
{
    if (pointCount <= 0 || pointCount > quadratures::Gauss::kMaxPointCount) {
        throw std::logic_error("Point count for Gauss quadrature must be in [1, " + std::to_string(quadratures::Gauss::kMaxPointCount) + "]");
    }

    for (int i = 0; i < pointCount; ++i) {
        auto x = quadratures::GaussKronrod::Abscissas[pointCount][i];
        auto scaledX = left + (x + 1) / 2 * (right - left);
        abscissas->push_back(scaledX);
        auto scaledW = quadratures::GaussKronrod::Weights[pointCount][i] * (right - left) / 2;
        weights->push_back(scaledW);
        auto scaledGaussW = quadratures::GaussKronrod::GaussWeights[pointCount][i] * (right - left) / 2;
        gaussWeights->push_back(scaledGaussW);
    }
}

static TQuadrature CreateGaussKronrodQuadrature(double left, double right, const TTIntegratorConfig& config)
{
    if (config.simple_node_count % 2 == 0) {
        throw std::logic_error("gauss_point_count must be odd, got " + std::to_string(config.simple_node_count));
    }
    if (config.node_count % config.simple_node_count != 0) {
        std::stringstream ss;
        ss << "quadrature_node_count must be divisible by quadrature_simple_node_count, "
           << "got (" << config.node_count << " - " << config.singularity_node_count << ") mod " << config.simple_node_count << " != 0";
        throw std::logic_error(ss.str());
    }
    if (config.singularity_node_count % config.simple_node_count != 0) {
        std::stringstream ss;
        ss << "quadrature_singularity_node_count must be divisible by quadrature_simple_node_count, "
           << "got (" << config.singularity_node_count << " - " << config.singularity_node_count << ") mod " << config.simple_node_count << " != 0";
        throw std::logic_error(ss.str());
    }

    std::vector<double> abscissas, weights, gaussWeights;

    auto fillIntervals = [&] (double left, double right, int pointCount, int gaussPointCount) {
        auto intervalCount = pointCount / gaussPointCount;
        auto intervalWidth = (right - left) / intervalCount;
        for (int i = 0; i < intervalCount; ++i) {
            FillGaussKronrod(
                left + intervalWidth * i,
                left + intervalWidth * (i + 1),
                gaussPointCount,
                &abscissas,
                &weights,
                &gaussWeights);
        }
    };

    auto singularityBorder = left + (right - left) * config.singularity_width;
    fillIntervals(left, singularityBorder, config.singularity_node_count, config.simple_node_count);
    fillIntervals(singularityBorder, right, config.node_count - config.singularity_node_count, config.simple_node_count);

    TQuadrature q;
    q.Abscissas.assign(1, std::move(abscissas));
    q.Weights.assign(1, std::move(weights));
    q.WeightsForErrorEstimate.emplace(1, std::move(gaussWeights));
    return q; 
}

using TFun = std::function<double(double)>;

static void ApplyTransform(TQuadrature* q, TFun transform, TFun transform_derivative)
{
    assert(q->Abscissas.size() == 1 && q->Weights.size() == 1);
    if (q->WeightsForErrorEstimate) {
        assert(q->WeightsForErrorEstimate->size() == 1);
    }

    auto& weights = q->Weights[0];
    auto& abscissas = q->Abscissas[0];
    for (int i = 0; i < static_cast<int>(abscissas.size()); ++i) {
        auto weight_multiplier = transform_derivative(abscissas[i]);
        weights[i] *= weight_multiplier;
        abscissas[i] = transform(abscissas[i]);
        if (q->WeightsForErrorEstimate) {
            (*q->WeightsForErrorEstimate)[0][i] *= weight_multiplier;
        }
    }
}

static std::pair<TFun, TFun> CreateTransform(const TTIntegratorConfig& config)
{
    switch (config.transform) {
        case ETransform::TR_IDENTITY:
            return {
                [](double x) { return x; },
                [](double x) { return 1; },
            };
        case ETransform::TR_POWER:
            return {
                [p=config.power_transform_power](double x) { return std::pow(x, p); },
                [p=config.power_transform_power](double x) { return std::pow(x, p - 1) * p; },
            };
    }
    std::cerr << "Unknown transform type: " << static_cast<int>(config.transform) << std::endl;
    std::abort();
}

static TQuadrature CreateOneDimensionalQuadrature(double left, double right, const TTIntegratorConfig& config)
{
    auto q = [&] {
        switch (config.type) {
            case EQuadratureType::QT_TRAPEZOIDAL:
                return CreateTrapezoidalQuadrature(left, right, config);
            case EQuadratureType::QT_TANH_SINH:
                return CreateTanhSinhQuadrature(left, right, config);
            case EQuadratureType::QT_SINGULARITY_MIXED:
                return CreateSingularityMixedQuadrature(left, right, config);
            case EQuadratureType::QT_GAUSS_MIXED:
                return CreateCompositeGaussMixedQuadrature(left, right, config);
            case EQuadratureType::QT_GAUSS_KRONROD:
                return CreateGaussKronrodQuadrature(left, right, config);
        }
        std::cerr << "Unknown quadrature type: " << static_cast<int>(config.type) << std::endl;
        std::abort();
    }();
    
    auto [transform, transform_derivative] = CreateTransform(config);
    ApplyTransform(&q, transform, transform_derivative);
    return q;
}

TQuadrature CreateQuadrature(const TBox& box, const TTIntegratorConfig& config)
{
    TQuadrature res;
    for (int k = 0; k < box.GetDim(); ++k) {
        auto oneDimQuadrature = CreateOneDimensionalQuadrature(box.Min[k], box.Max[k], config);
        res.Abscissas.push_back(std::move(oneDimQuadrature.Abscissas[0]));
        res.Weights.push_back(std::move(oneDimQuadrature.Weights[0]));
        if (k == 0 && oneDimQuadrature.WeightsForErrorEstimate) {
            res.WeightsForErrorEstimate.emplace();
        }
        if (res.WeightsForErrorEstimate) {
            assert(oneDimQuadrature.WeightsForErrorEstimate);
            res.WeightsForErrorEstimate->push_back(std::move((*oneDimQuadrature.WeightsForErrorEstimate)[0]));
        }
    }
    return res;
}
