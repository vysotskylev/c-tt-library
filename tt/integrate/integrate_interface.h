struct TTIntegratorConfig;

template <typename TArg, typename TRes>
using TTFun = void (*)(int count, const TArg* points, TRes* results, void* userdata);

template <typename TArg, typename TRes>
int TTIntegrateFunPtr(
    int integral_dimension,
    const struct TTIntegratorConfig* config,
    TTFun<TArg, TRes> fun,
    TRes* integral,
    TRes* error,
    void* userdata);
