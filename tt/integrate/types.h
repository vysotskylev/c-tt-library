#pragma once

#include <vector>

struct TBox
{
    int GetDim() const
    {
        return Min.size();
    }

    std::vector<double> Min, Max;
};
