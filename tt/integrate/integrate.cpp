#include "integrate.h"
#include "integrate_interface.h"

#include <complex>

template <typename TArg, typename TRes>
int TTIntegrateFunPtr(
    int integral_dimension,
    const TTIntegratorConfig* config,
    TTFun<TArg, TRes> fun,
    TRes* integral,
    TRes* error,
    void* userdata)
{
    std::vector<TRes> actual_result;
    auto integrate_fun = [&] (int count, TArg* points, TRes* result) {
        actual_result.resize(count);
        fun(count, points, actual_result.data(), userdata);
        for (auto point_idx = 0; point_idx < count; ++point_idx) {
            result[point_idx] = actual_result[point_idx];
        }
    };
    TBox box{
        std::vector<double>(integral_dimension, 0.0),
        std::vector<double>(integral_dimension, 1.0),
    };
 
    auto res = TTIntegrate<TArg, TRes>(integrate_fun, box, *config);

    *integral = res.value;
    *error = res.error_estimate.value_or(0.0);

    return 1;
}

template
int TTIntegrateFunPtr<float, float>(int integral_dimension, const TTIntegratorConfig* config, TTFun<float, float> fun, float* integral, float* error, void* userdata);

template
int TTIntegrateFunPtr<double, double>(int integral_dimension, const TTIntegratorConfig* config, TTFun<double, double> fun, double* integral, double* error, void* userdata);

template
int TTIntegrateFunPtr<float, std::complex<float>>(int integral_dimension, const TTIntegratorConfig* config, TTFun<float, std::complex<float>> fun, std::complex<float>* integral, std::complex<float>* error, void* userdata);

template
int TTIntegrateFunPtr<double, std::complex<double>>(int integral_dimension, const TTIntegratorConfig* config, TTFun<double, std::complex<double>> fun, std::complex<double>* integral, std::complex<double>* error, void* userdata);
