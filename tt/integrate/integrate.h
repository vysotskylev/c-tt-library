#pragma once

#include "types.h"
#include "quadrature.h"

#include <tt/core/tensor_train.h>
#include <tt/core/assertion.h>

#include <array>
#include <vector>
#include <optional>

#include <iostream>
#include <fstream>
#include <sstream>

#include <chrono>
#include <ctime>

template <typename T>
struct ComplexTraits {
    static constexpr bool IsComplex = false;
};

template <typename T>
struct ComplexTraits<std::complex<T>> {
    static constexpr bool IsComplex = true;
    using BaseType = T;
};

inline std::string GetTimeString()
{
    std::stringstream ss;
    auto t = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    ss << std::ctime(&t);
    return ss.str();
}

template <typename T, typename Fun>
class TFunTensor
    : public TTensor<T>
{
public:
    TFunTensor(Fun f, std::vector<int> modes_sizes)
        : TTensor<T>(std::move(modes_sizes))
        , F_(f)
    { }

    void ComputeAt(int count, const int* indices, T* result) const override
    {
        F_(count, indices, result);
    }

private:
    Fun F_;
};

template <typename T>
struct TTIntegrationResult {
    T value;
    std::optional<T> error_estimate;
};

template <typename TArg, typename TRes, typename Fun>
TTIntegrationResult<TRes> TTIntegrate(
    Fun f,
    const TBox& box,
    const TTIntegratorConfig& config)
{
    const int dim = box.GetDim();

    auto quadrature = CreateQuadrature(box, config);

    std::vector<TArg> args;
    auto boxed_fun = [&] (int count, const int* indices, TRes* result) {
        //called++;
        args.clear();
        args.reserve(count * dim);
        for (auto point_idx = 0; point_idx < count; ++point_idx) {
            for (int i = 0; i < dim; ++i) {
                args.push_back(quadrature.Abscissas[i][indices[point_idx * dim + i]]);
            }
        }
        f(count, args.data(), result);
    };

    std::vector<int> step_counts(dim, config.node_count);
    auto fun_tensor = TFunTensor<TRes, decltype(boxed_fun)>(boxed_fun, step_counts);

    TTensorTrainParameters params;
    params.tolerance = config.approximation_tolerance;
    params.maximal_iterations_number = config.approximation_iteration_count;
    params.evaluation_count_soft_limit = config.evaluation_count_soft_limit;
    params.evaluation_count_hard_limit = config.evaluation_count_hard_limit;
    auto approximation_result = TTensorTrain<TRes>::ApproximateWithEstimate(&fun_tensor, params);

    std::vector<std::vector<TRes>> weights;
    for (const auto& w : quadrature.Weights) {
        weights.emplace_back(w.begin(), w.end());
    }
    auto weights_train = TTensorTrain<TRes>::MakeRankOne(weights);
    auto value = approximation_result.approximation.Dot(weights_train);

    if (!quadrature.WeightsForErrorEstimate.has_value()) {
        return {value, std::nullopt};
    }

    double weight_tensor_norm_square = 1;
    for (const auto& weights : quadrature.Weights) {
        double norm_square = 0;
        for (auto w : weights) {
            norm_square += w * w;
        }
        weight_tensor_norm_square *= norm_square;
    }
    auto approximation_error = approximation_result.error_estimate * std::sqrt(weight_tensor_norm_square);

    std::vector<std::vector<TRes>> weights_for_error_estimate;
    for (const auto& w : *quadrature.WeightsForErrorEstimate) {
        weights_for_error_estimate.emplace_back(w.begin(), w.end());
    }
    auto anchor_train = TTensorTrain<TRes>::MakeRankOne(weights_for_error_estimate);
    auto anchor_value = approximation_result.approximation.Dot(anchor_train);

    TRes error;
    if constexpr (ComplexTraits<TRes>::IsComplex) {
        auto integration_error = value - anchor_value;
        using BaseType = typename ComplexTraits<TRes>::BaseType;
        error = TRes{
            static_cast<BaseType>(std::abs(integration_error.real()) + approximation_error),
            static_cast<BaseType>(std::abs(integration_error.imag()) + approximation_error),
        };
    } else {
        error = std::abs(value - anchor_value) + approximation_error;
    }

    return {value, error};
}
