#pragma once

#include "matrix.h"

#include <vector>

template <typename T>
class TTensor
{
public:
    using Value = T;

    TTensor(int dimensionality, const int* modes_sizes);
    TTensor(std::vector<int> modes_sizes = {});
    virtual ~TTensor() = default;

    Value operator[](const int *indices) const;
    Value operator[](const std::vector<int> &indices) const;

    virtual void ComputeAt(int count, const int* indices, Value* result) const = 0;

    int get_dimensionality() const;
    int get_mode_size(int) const;
    std::vector<int> get_modes_sizes() const;

protected:
    std::vector<int> modes_sizes;
};

// Matrix of size m x n with elements
// U(i,j) = tensor[r[i][0], ..., r[i][k-1], c[j][d-k-1], ..., c[j][0]].
template <typename T>
class TUnfoldingSubMatrix
    : public TMatrix<T>
{
public:
    using Value = T;

    TUnfoldingSubMatrix(
        const TTensor<Value>* tensor,
        int unfolding_index,
        const std::vector<std::vector<int>>& row_indices,
        const std::vector<std::vector<int>>& column_indices);

    void ComputeAt(int count, const int* indices, Value* result) const override;

private:
    const TTensor<Value>* const tensor_;
    const int unfolding_index_;
    const std::vector<std::vector<int>>& row_indices_;
    const std::vector<std::vector<int>>& column_indices_;

    mutable std::vector<int> tensor_indices_;
};
