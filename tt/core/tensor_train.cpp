#include "tensor_train.h"

#include "assertion.h"
#include "contrib/uvector/uvector.h"
#include "lapack_wrap.h"
#include "simple_cross.h"

#include <algorithm>
#include <iostream>
#include <fstream>

#include <random>
#include <set>

#include <cstdlib>
#include <cstdio>
#include <cmath>

using std::vector;

template <typename T>
TTensorTrain<T>::TTensorTrain(const TTensorTrain &tens, Value c)
    : TTensor<Value>(tens.get_modes_sizes())
    , ranks(tens.get_dimensionality() + 1, 1)
    , carriages(tens.get_dimensionality())
{
    ENSURE(this->modes_sizes.size() > 0);
    for (int dim = 0; dim < this->get_dimensionality(); dim++) {
        carriages[dim].assign(this->modes_sizes[dim], c);
    }
}

template <typename T>
TTensorTrain<T>::TTensorTrain(const TTensorTrain &tens, const Value *c)
    : TTensor<Value>(tens.get_modes_sizes())
    , ranks(tens.get_dimensionality() + 1, 1)
    , carriages(tens.get_dimensionality())
{
    ENSURE(this->modes_sizes.size() > 0);
    for (int dim = 0; dim < this->get_dimensionality(); dim++) {
        carriages[dim].assign(c, c + this->modes_sizes[dim]);
    }
}

template <typename T>
void TTensorTrain<T>::SaveToFile(const char *filename) const
{
    FILE *f = fopen(filename, "wb");
    int dimensionality = this->get_dimensionality();
    fwrite(&dimensionality, sizeof(dimensionality), 1, f);
    fwrite(this->modes_sizes.data() , sizeof(*this->modes_sizes.data()), this->get_dimensionality(), f);
    fwrite(ranks.data(), sizeof(*ranks.data()), this->get_dimensionality() + 1, f);
    for (int dim = 0; dim < this->get_dimensionality(); dim++)
    {
        fwrite(carriages[dim].data(), sizeof(*carriages[dim].data()), ranks[dim] * this->modes_sizes[dim] * ranks[dim + 1], f);
    }
    fclose(f);
}

template <typename T>
void TTensorTrain<T>::LoadFromFile(const char *filename)
{
    FILE *f = fopen(filename, "rb");
    int dimensionality;
    ENSURE(fread(&dimensionality, sizeof(dimensionality), 1, f) == 1);
    this->modes_sizes.resize(dimensionality);
    ranks.resize(dimensionality + 1);
    carriages.resize(dimensionality);
    ENSURE(static_cast<int>(fread(this->modes_sizes.data(), sizeof(*this->modes_sizes.data()), dimensionality, f)) == dimensionality);
    ENSURE(static_cast<int>(fread(ranks.data(), sizeof(*ranks.data()), dimensionality + 1, f)) == dimensionality + 1);
    for (int dim = 0; dim < dimensionality; dim++) {
        carriages.resize(ranks[dim] * this->modes_sizes[dim] * ranks[dim + 1]);
        auto count = ranks[dim] * this->modes_sizes[dim] * ranks[dim + 1];
        ENSURE(static_cast<int>(fread(carriages[dim].data(), sizeof(*carriages[dim].data()), count, f)) == count);
    }
    fclose(f);
}

template <typename T>
double TTensorTrain<T>::Norm2() const
{
    return sqrt(std::abs(Dot(*this)));
}

template <typename T>
void TTensorTrain<T>::ComputeAt(int count, const int *indices, Value* result) const
{
    auto dimensionality = this->get_dimensionality();
    for (auto point_idx = 0; point_idx < count; ++point_idx) {
        result[point_idx] = ComputeAtPoint(indices + point_idx * dimensionality);
    }
}

template <typename T>
T TTensorTrain<T>::ComputeAtPoint(const int* indices) const
{
    int max_rank = *std::max_element(ranks.begin(), ranks.end());

    ao::uvector<Value> row(max_rank);
    row[0] = 1.0;
    ao::uvector<Value> result(max_rank);

    for (int dim = 0; dim < this->get_dimensionality(); dim++) {
        gemv<Value>(
            CblasColMajor,
            CblasNoTrans,
            ranks[dim + 1],
            ranks[dim],
            1.0,
            carriages[dim].data() + indices[dim] * ranks[dim] * ranks[dim + 1],
            ranks[dim + 1],
            row.data(),
            1,
            0.0,
            result.data(),
            1);
        std::swap(row, result);
    }
    return row[0];
}

template <typename T>
T TTensorTrain<T>::operator[](int multiindex) const
{
    int indices[this->get_dimensionality()];
    for (int i = 0; i < this->get_dimensionality(); i++) {
        indices[i] = multiindex % this->modes_sizes[i];
        multiindex /= this->modes_sizes[i];
        // it's correct
    }
    return ComputeAtPoint(indices);
}

template <typename T>
class TTensorTrain<T>::TApproximator
{
public:
    TApproximator(TTensor<Value>* tensor, const TTensorTrainParameters& parameters)
        : tensor_(tensor)
        , parameters_(parameters)
        , dim_(tensor_->get_dimensionality())
        , mode_sizes_(tensor_->get_modes_sizes())
        , left_indices_(dim_ + 1)
        , right_indices_(dim_ + 1)
        , back_left_indices_(dim_ + 1)
        , back_right_indices_(dim_ + 1)
    { 
        forward_.modes_sizes = mode_sizes_;
        forward_.ranks.assign(dim_ + 1, 0);
        forward_.carriages.resize(dim_);
        backward_ = forward_;
        for (int k = 0; k < dim_; ++k) {
            index_distributions_.emplace_back(0, mode_sizes_[k] - 1);
        }
    }

    // Attention! Heuristics ahead.
    //
    // We assume that TT-ranks are proportional to log(1/tol).
    static double ComputeEffectiveTolerance(
        double desired_tolerance,
        double test_tolerance,
        int64_t remaining_evaluation_count,
        int64_t test_evaluation_count)
    {
        auto ratio = static_cast<double>(remaining_evaluation_count) / test_evaluation_count; 
        auto heuristic_tolerance = std::pow(test_tolerance, std::sqrt(ratio));
        return std::max(heuristic_tolerance, desired_tolerance);
    }

    double ComputeCrossApproximationTolerance(double tt_tolerance) const
    {
        return tt_tolerance / std::sqrt(static_cast<double>(dim_));
    }

    struct TToleranceCheckResult {
        double difference_frobenius_norm;
        double relative_difference_frobenius_norm;
        double tolerance;

        bool IsBelowTolerance() const {
            return relative_difference_frobenius_norm <= tolerance;
        }
    };

    bool ShouldStop(const TToleranceCheckResult& result) const
    {
        return result.IsBelowTolerance() || GetRemainingEvaluationCount() == 0;
    }

    TApproximationResult Approximate()
    {
        if (dim_ == 1) {
            forward_.ranks = {1, 1};
            forward_.carriages[0].resize(forward_.modes_sizes[0]);
            for (int i = 0; i < forward_.modes_sizes[0]; ++i) {
                forward_.carriages[0][i] = (*tensor_)[&i];
            }
            return TApproximationResult{forward_, 0.0};
        }

        double test_tolerance = 1e-2;
        RunForwardPass(test_tolerance);
        auto effective_tolerance = ComputeEffectiveTolerance(
            parameters_.tolerance,
            test_tolerance,
            GetRemainingEvaluationCount(),
            evaluation_count_);
        os_ << "eval_count = " << evaluation_count_ << ", eff_tol = " << effective_tolerance << std::endl;

        RunBackwardPass(effective_tolerance);
        if (auto res = CheckTolerance(backward_, forward_, effective_tolerance); ShouldStop(res)) {
            os_ << "Done, remaining_eval_count: " << GetRemainingEvaluationCount() << std::endl;
            return TApproximationResult{backward_, res.difference_frobenius_norm};
        }

        int iters = 1;
        while ((iters < parameters_.maximal_iterations_number) || (parameters_.maximal_iterations_number == 0)) {
            RunForwardPass(effective_tolerance);

            if (auto res = CheckTolerance(forward_, backward_, effective_tolerance); ShouldStop(res)) {
                os_ << "Done, remaining_eval_count: " << GetRemainingEvaluationCount() << std::endl;
                return TApproximationResult{forward_, res.difference_frobenius_norm};
            }

            RunBackwardPass(effective_tolerance);

            if (auto res = CheckTolerance(backward_, forward_, effective_tolerance); ShouldStop(res)) {
                os_ << "Done, remaining_eval_count: " << GetRemainingEvaluationCount() << std::endl;
                return TApproximationResult{backward_, res.difference_frobenius_norm};
            }

            iters++;
        }
        os_ << "Max iterations (" << parameters_.maximal_iterations_number << ") exhausted" << std::endl;
        auto res = CheckTolerance(backward_, forward_, effective_tolerance);
        return TApproximationResult{backward_, res.difference_frobenius_norm};
    }

private:
    using TMultiIndex = vector<int>;

private:
    const TTensor<Value>* const tensor_;
    const TTensorTrainParameters& parameters_;
    const int dim_;
    const vector<int> mode_sizes_;
    TTensorTrain forward_;
    TTensorTrain backward_;
    vector<vector<TMultiIndex>> left_indices_;
    vector<vector<TMultiIndex>> right_indices_;
    vector<vector<TMultiIndex>> back_left_indices_;
    vector<vector<TMultiIndex>> back_right_indices_;
    int64_t evaluation_count_ = 0;
    std::mt19937 random_generator_{23};
    std::vector<std::uniform_int_distribution<int>> index_distributions_;

    std::ofstream os_{"/home/levysotsky/Science/tt/fiesta/FIESTA4/approximate-log", std::ios_base::app};

private:
    int64_t GetRemainingEvaluationCount() const
    {
        return std::max<int64_t>(0, parameters_.evaluation_count_soft_limit - evaluation_count_);
    }

    int64_t GetRemainingEvaluationCountHardLimit() const
    {
        return std::max<int64_t>(0, parameters_.evaluation_count_hard_limit - evaluation_count_);
    }

    TCrossParallel<Value> ComputeCrossApproximation(
        const TMatrix<Value>* matrix,
        bool start_from_column,
        int max_rank,
        double tolerance)
    {
        TCrossParallelParameters params;
        params.tolerance = ComputeCrossApproximationTolerance(tolerance);
        params.maximal_iterations_number = parameters_.cross_max_iterations;
        params.number_of_checked_elements = 1;
        params.stop_rank = parameters_.stop_rank;
        params.max_rank = max_rank;
        params.start_from_column = start_from_column;
        params.evaluation_count_hard_limit = GetRemainingEvaluationCountHardLimit();

        auto result = TCrossParallel<Value>(matrix, std::move(params));
        result.Approximate();
        evaluation_count_ += result.GetEvaluationCount();
        return result;
    }

    void RunForwardPass(double tolerance)
    {
        forward_.ranks[0] = 1;
        forward_.ranks[dim_] = 1;
        for (int k = 1; k < dim_; k++) {
            left_indices_[k].clear();
            left_indices_[k].resize(forward_.ranks[k - 1] * mode_sizes_[k - 1]);
            // Make n_{k-1} * r_{k-1} left indices from r_{k-1} previous left indices.
            for (int i = 0; i < mode_sizes_[k - 1]; i++) {
                for (int j = 0; j < forward_.ranks[k - 1]; j++) {
                    if (k > 1) {
                        left_indices_[k][i * forward_.ranks[k - 1] + j] = left_indices_[k - 1][j];
                    }
                    left_indices_[k][i * forward_.ranks[k - 1] + j].push_back(i);
                }
            }
            if (k != dim_ - 1) {
                size_t new_size = std::max(
                    right_indices_[k].size() + back_right_indices_[k].size() + right_indices_[k - 1].size(),
                    left_indices_[k].size());
                right_indices_[k].reserve(new_size);
                right_indices_[k].insert(right_indices_[k].end(), back_right_indices_[k].begin(), back_right_indices_[k].end());
                for (int i = 0; i < static_cast<int>(right_indices_[k - 1].size()); ++i) {
                    right_indices_[k].emplace_back(right_indices_[k - 1][i].begin(), right_indices_[k - 1][i].begin() + dim_ - k);
                }

                for (int i = right_indices_[k].size(); i < static_cast<int>(left_indices_[k].size()); i++) {
                    right_indices_[k].push_back(GetRandomMultiIndex(k, dim_));
                }
            } else {
                right_indices_[k].resize(mode_sizes_[dim_ - 1]);
                for (int i = 0; i < mode_sizes_[dim_ - 1];  i++) {
                    right_indices_[k][i].assign(1, i);
                }
            }
            TUnfoldingSubMatrix<Value> work_matrix(tensor_, k, left_indices_[k], right_indices_[k]);
            auto cross_approx = ComputeCrossApproximation(
                &work_matrix,
                /* start_from_column */ true,
                forward_.ranks[k],
                tolerance);

            forward_.ranks[k] = cross_approx.GetRank();
            forward_.carriages[k - 1].resize(forward_.ranks[k - 1] * forward_.ranks[k] * mode_sizes_[k - 1]);
            gemm<T>(
                CblasColMajor,
                CblasTrans,
                CblasTrans,
                forward_.ranks[k],
                forward_.ranks[k - 1] * mode_sizes_[k - 1],
                forward_.ranks[k],
                1.0,
                cross_approx.export_hat_A_inv(),
                forward_.ranks[k],
                cross_approx.export_C(),
                forward_.ranks[k - 1] * mode_sizes_[k - 1],
                0.0,
                forward_.carriages[k - 1].data(),
                forward_.ranks[k]);

            std::vector<std::vector<int>> new_left_indices, new_right_indices;
            new_left_indices.reserve(forward_.ranks[k]);
            new_right_indices.reserve(forward_.ranks[k]);
            for (int i = 0; i < forward_.ranks[k]; i++) {
                new_left_indices.push_back(std::move(left_indices_[k][cross_approx.GetRowPosition(i)]));
                new_right_indices.push_back(std::move(right_indices_[k][cross_approx.GetColumnPosition(i)]));
            }
            left_indices_[k] = std::move(new_left_indices);
            right_indices_[k] = std::move(new_right_indices);

            if (k == dim_ - 1) {
                forward_.carriages[dim_ - 1].resize(forward_.ranks[dim_ - 1] * forward_.ranks[dim_] * mode_sizes_[dim_ - 1]);
                for (int i = 0; i < forward_.ranks[dim_ - 1] * forward_.ranks[dim_]; i++) {
                    copy(
                        mode_sizes_[dim_ - 1],
                        cross_approx.export_RT() + i * mode_sizes_[dim_ - 1],
                        1,
                        forward_.carriages[dim_ - 1].data() + i,
                        forward_.ranks[dim_ - 1] * forward_.ranks[dim_]);
                }
            }
        }
    }

    void RunBackwardPass(double tolerance)
    {
        backward_.ranks[0] = 1;
        backward_.ranks[dim_] = 1;
        for (int k = dim_ - 1; k > 0; k--) {
            back_right_indices_[k].clear();
            back_right_indices_[k].resize(backward_.ranks[k + 1] * mode_sizes_[k]);
            for (int i = 0; i < mode_sizes_[k]; i++) {
                for (int j = 0; j < backward_.ranks[k + 1]; j++) {
                    if (k < dim_ - 1) {
                        back_right_indices_[k][i * backward_.ranks[k + 1] + j] = back_right_indices_[k + 1][j];
                    }
                    back_right_indices_[k][i * backward_.ranks[k + 1] + j].push_back(i);
                }
            }
            if (k != 1) {
                size_t new_size = std::max(
                    back_left_indices_[k].size() + left_indices_[k].size() + back_left_indices_[k + 1].size(),
                    back_right_indices_[k].size());
                back_left_indices_[k].reserve(new_size);
                back_left_indices_[k].insert(back_left_indices_[k].end(), left_indices_[k].begin(), left_indices_[k].end());

                for (int i = 0; i < static_cast<int>(back_left_indices_[k + 1].size()); i++) {
                    back_left_indices_[k].emplace_back(back_left_indices_[k + 1][i].begin(), back_left_indices_[k + 1][i].begin() + k);
                }
                for (int i = back_left_indices_[k].size(); i < static_cast<int>(back_right_indices_[k].size()); i++) {
                    back_left_indices_[k].push_back(GetRandomMultiIndex(0, k));
                }
            } else {
                back_left_indices_[k].resize(mode_sizes_[0]);
                for (int i = 0; i < mode_sizes_[0]; ++i) {
                    back_left_indices_[k][i].assign(1, i);
                }
            }
            TUnfoldingSubMatrix<Value> work_matrix(tensor_, k, back_left_indices_[k], back_right_indices_[k]);
            auto cross_approx = ComputeCrossApproximation(
                &work_matrix,
                /* start_from_column */ false,
                backward_.ranks[k + 1],
                tolerance);
            backward_.ranks[k] = cross_approx.GetRank();
            backward_.carriages[k].resize(backward_.ranks[k + 1] * backward_.ranks[k] * mode_sizes_[k]);
            for (int i = 0; i < mode_sizes_[k]; i++) {
                gemm<T>(
                    CblasColMajor,
                    CblasNoTrans,
                    CblasTrans,
                    backward_.ranks[k + 1],
                    backward_.ranks[k],
                    backward_.ranks[k],
                    1.0,
                    cross_approx.export_RT() + i * backward_.ranks[k + 1],
                    backward_.ranks[k + 1] * mode_sizes_[k],
                    cross_approx.export_hat_A_inv(),
                    backward_.ranks[k],
                    0.0,
                    backward_.carriages[k].data() + i * backward_.ranks[k] * backward_.ranks[k + 1],
                    backward_.ranks[k + 1]);
            }

            vector<vector<int>> new_back_left_indices, new_back_right_indices;
            new_back_left_indices.reserve(backward_.ranks[k]);
            new_back_right_indices.reserve(backward_.ranks[k]);
            for (int i = 0; i < backward_.ranks[k]; i++) {
                new_back_left_indices.push_back(std::move(back_left_indices_[k][cross_approx.GetRowPosition(i)]));
                new_back_right_indices.push_back(std::move(back_right_indices_[k][cross_approx.GetColumnPosition(i)]));
            }
            back_left_indices_[k] = std::move(new_back_left_indices);
            back_right_indices_[k] = std::move(new_back_right_indices);
            if (k == 1) {
                backward_.carriages[0].resize(backward_.ranks[1] * backward_.ranks[0] * mode_sizes_[0]);
                for (int i = 0; i < backward_.ranks[0] * backward_.ranks[1]; i++) {
                    copy(
                        mode_sizes_[0],
                        cross_approx.export_C() + i * mode_sizes_[0],
                        1,
                        backward_.carriages[0].data() + i,
                        backward_.ranks[0] * backward_.ranks[1]);
                }
            }
        }
    }

    TMultiIndex GetRandomMultiIndex(int mode_begin, int mode_end)
    {
        TMultiIndex mi;
        mi.reserve(mode_end - mode_begin);
        for (int k = mode_begin; k < mode_end; ++k) {
            mi.push_back(index_distributions_[k](random_generator_));
        }
        return mi;
    }

    TToleranceCheckResult CheckTolerance(const TTensorTrain& current, const TTensorTrain& previous, double tolerance)
    {
        auto current_sq_norm = Real(current.Dot(current));
        auto previous_sq_norm = Real(previous.Dot(previous));

        auto diff_sq_norm = current_sq_norm + previous_sq_norm - 2.0 * Real(current.Dot(previous));
        // os_ << "Eval count: " << evaluation_count_ << std::endl;
        // os_ << "Tolerance: " << std::sqrt(diff_sq_norm / current_sq_norm) << std::endl;
        // os_ << "Ranks: ";
        // for (int i = 1; i < dim_; ++i) {
        //     os_ << forward_.ranks[i] << ", ";
        // }
        // os_ << std::endl;
        TToleranceCheckResult result;
        result.difference_frobenius_norm = std::sqrt(std::abs(diff_sq_norm));
        result.relative_difference_frobenius_norm = result.difference_frobenius_norm / std::sqrt(current_sq_norm);
        result.tolerance = tolerance;
        return result;
    }
};

template <typename T>
typename TTensorTrain<T>::TApproximationResult
TTensorTrain<T>::ApproximateWithEstimate(TTensor<Value> *tensor, const TTensorTrainParameters &parameters)
{
    TApproximator approximator(tensor, parameters);
    return approximator.Approximate();
}

template <typename T>
TTensorTrain<T> TTensorTrain<T>::Approximate(TTensor<Value> *tensor, const TTensorTrainParameters &parameters)
{
    return ApproximateWithEstimate(tensor, parameters).approximation;
}

template <typename T>
TTensorTrain<T> TTensorTrain<T>::MakeRankOne(const std::vector<std::vector<Value>>& factors)
{
    TTensorTrain tt;
    for (const auto& factor : factors) {
        tt.modes_sizes.push_back(factor.size());
        tt.carriages.emplace_back(factor);
    }
    tt.ranks.assign(factors.size() + 1, 1);
    return tt;
}

template <typename T>
int TTensorTrain<T>::GetRank(int i) const
{
    return ranks[i];
}

template <typename T>
T TTensorTrain<T>::Dot(const TTensorTrain &other) const
{
    ENSURE(this->get_modes_sizes() == other.get_modes_sizes());

    std::vector<T> v(1, 1.0);
    for (int dim = 0; dim < this->get_dimensionality(); dim++) {
        ao::uvector<T> g(ranks[dim] * other.ranks[dim + 1]);
        std::vector<T> v1(ranks[dim + 1] * other.ranks[dim + 1], 0.0);
        for (int i = 0; i < this->modes_sizes[dim]; i++) {
            gemm<T>(
                CblasColMajor,
                CblasNoTrans,
                CblasConjTrans,
                ranks[dim],
                other.ranks[dim + 1],
                other.ranks[dim],
                1.0,
                v.data(),
                ranks[dim],
                other.carriages[dim].data() + i * other.ranks[dim] * other.ranks[dim + 1],
                other.ranks[dim + 1],
                0.0,
                g.data(),
                ranks[dim]);
            gemm<T>(
                CblasColMajor,
                CblasNoTrans,
                CblasNoTrans,
                ranks[dim + 1],
                other.ranks[dim + 1],
                ranks[dim],
                1.0,
                carriages[dim].data() + i * ranks[dim] * ranks[dim + 1],
                ranks[dim + 1],
                g.data(),
                ranks[dim],
                1.0,
                v1.data(),
                ranks[dim + 1]);
        }
        std::swap(v, v1);
    }
    return v[0];
}

template <typename T>
TTensorTrain<T> TTensorTrain<T>::operator+(const TTensorTrain &other)  const
{
    ENSURE(this->get_modes_sizes() == other.get_modes_sizes());

    TTensorTrain sum;
    sum.modes_sizes = this->modes_sizes;
    sum.ranks = ranks;
    for (int dim = 1; dim < this->get_dimensionality(); ++dim) {
        sum.ranks[dim] += other.ranks[dim];
    }
    sum.carriages.resize(this->get_dimensionality());
    sum.carriages[0].resize(sum.modes_sizes[0] * sum.ranks[1]);
    for (int j = 0; j < sum.modes_sizes[0]; ++j) {
        copy(ranks[1], carriages[0].data() + j * ranks[1], 1, sum.carriages[0].data() + j * sum.ranks[1], 1);
        copy(other.ranks[1], other.carriages[0].data() + j * other.ranks[1], 1, sum.carriages[0].data() + j * sum.ranks[1] + ranks[1], 1);
    }

    for (int dim = 1; dim < this->get_dimensionality() - 1; ++dim) {
        sum.carriages[dim].resize(sum.modes_sizes[dim] * sum.ranks[dim] * sum.ranks[dim+1], 0.0);
        for (int j = 0; j < sum.modes_sizes[dim]; ++j) {
            for (int alpha = 0; alpha < ranks[dim]; alpha++) {
                copy(
                    ranks[dim + 1],
                    carriages[dim].data() + j * ranks[dim] * ranks[dim + 1] + alpha * ranks[dim + 1],
                    1,
                    sum.carriages[dim].data() + j * sum.ranks[dim] * sum.ranks[dim + 1] + alpha * sum.ranks[dim + 1],
                    1);
            }
            for (int alpha = ranks[dim]; alpha < sum.ranks[dim]; alpha++) {
                copy(
                    other.ranks[dim + 1],
                    other.carriages[dim].data() + j * other.ranks[dim] * other.ranks[dim+1] + (alpha - ranks[dim]) * other.ranks[dim + 1],
                    1,
                    sum.carriages[dim].data() + j * sum.ranks[dim] * sum.ranks[dim + 1] + alpha * sum.ranks[dim + 1] + ranks[dim + 1],
                    1);
            }
        }
    }

    int dim = this->get_dimensionality() - 1;
    sum.carriages[dim].resize(sum.modes_sizes[dim] * sum.ranks[dim]);
    for (int j = 0; j < sum.modes_sizes[dim]; ++j) {
        copy(ranks[dim], carriages[dim].data() + j * ranks[dim], 1, sum.carriages[dim].data() + j * sum.ranks[dim], 1);
        copy(other.ranks[dim], other.carriages[dim].data() + j * other.ranks[dim], 1, sum.carriages[dim].data() + j * sum.ranks[dim] + ranks[dim], 1);
    }
    return sum;
}

template <typename T>
TTensorTrain<T> TTensorTrain<T>::operator-(const TTensorTrain &tensor) const
{
    return *this + (tensor * (-1.0));
}

template <typename T>
TTensorTrain<T> TTensorTrain<T>::operator*(Value alpha) const
{
    int dim = this->get_dimensionality() - 1;
    int lastsize = this->modes_sizes[dim] * ranks[dim] * ranks[dim + 1];
    TTensorTrain new_tensor = *this;
    scal(lastsize, alpha, new_tensor.carriages[dim].data(), 1);
    return new_tensor;
}

template
class TTensorTrain<double>;
template
class TTensorTrain<float>;
template
class TTensorTrain<std::complex<double>>;
template
class TTensorTrain<std::complex<float>>;

template <typename T>
static void MaterializeTensor(
    const TTensor<T>& tensor,
    const std::vector<int>& leading_sizes,
    int dim, 
    std::vector<int>& indices,
    T* result)
{
    const auto mode_size = tensor.get_mode_size(dim);
    if (dim == 0) {
        for (int i = 0; i < mode_size; ++i) {
            indices[0] = i;
            result[i] = tensor[indices];
        }
        return;
    }
    const auto leading_size = leading_sizes[dim - 1];
    for (int i = 0; i < mode_size; ++i) {
        indices[dim] = i;
        MaterializeTensor(tensor, leading_sizes, dim - 1, indices, result + leading_size * i);
    }
}

template <typename T>
std::vector<std::vector<Underlying<T>>> ComputeUnfoldingMatrixSingularValues(const TTensor<T>& tensor)
{
    static constexpr int kMaxSize = 100000;
    const auto& mode_sizes = tensor.get_modes_sizes();
    if (mode_sizes.size() < 2) {
        return {};
    }
    std::vector<int> leading_sizes = {mode_sizes[0]};
    for (int k = 1; k < tensor.get_dimensionality() - 1; ++k) {
        leading_sizes.push_back(leading_sizes.back() * mode_sizes[k]);
        ENSURE(leading_sizes.back() <= kMaxSize);
    }
    auto size = leading_sizes.back() * mode_sizes.back();
    ao::uvector<T> materialized(size);
    std::vector<int> indices(tensor.get_dimensionality());
    MaterializeTensor(tensor, leading_sizes, tensor.get_dimensionality() - 1, indices, materialized.data());
    std::vector<std::vector<Underlying<T>>> singular_values;
    for (int k = 0; k < tensor.get_dimensionality() - 1; ++k) {
        auto row_count = leading_sizes[k];
        auto column_count = size / row_count;
        std::vector<Underlying<T>> values(std::min(row_count, column_count));
        auto copy = materialized;
        compute_singular_numbers(row_count, column_count, copy.data(), values.data());
        singular_values.push_back(std::move(values));
    }
    return singular_values;
}

template
std::vector<std::vector<double>> ComputeUnfoldingMatrixSingularValues(const TTensor<double>& tensor);
template
std::vector<std::vector<float>> ComputeUnfoldingMatrixSingularValues(const TTensor<float>& tensor);
template
std::vector<std::vector<double>> ComputeUnfoldingMatrixSingularValues(const TTensor<std::complex<double>>& tensor);
template
std::vector<std::vector<float>> ComputeUnfoldingMatrixSingularValues(const TTensor<std::complex<float>>& tensor);