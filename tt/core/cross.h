#pragma once

#include "matrix.h"

#include <vector>

template <typename T>
class TVolume
{
public:
    TVolume(int size, T volume, int* rows, int* columns);
    explicit TVolume(int size);

    void Set(T volume, int* rows, int* columns);

    T GetVolume() const;
    int GetRowPosition() const;
    int GetColumnPosition() const;

    bool operator>(const TVolume &) const;
    bool operator==(const TVolume &) const;
    bool operator!=(const TVolume &) const;

private:
    T volume_;
    std::vector<int> rows_;
    std::vector<int> columns_;
};

class TCrossBase
{
public:
    void Approximate();

private:
    virtual void PrepareData() = 0;
    virtual void SearchMaxVolume() = 0;
    virtual bool StoppingCriterionSatisfied() const = 0;
    virtual void UpdateCross() = 0;
    virtual void ComputeApproximation() = 0;
};
