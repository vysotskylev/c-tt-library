#include "utils.h"

#include <fstream>
#include <iostream>

void WriteMatrix(int rows, int cols, const double* matrix, const std::string& path)
{
    std::ofstream os(path);
    if (!os.is_open()) {
        std::cerr << "Failed to open file " + path << std::endl;
        exit(1);
    }
    os.precision(16);
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            os << matrix[j * rows + i] << ' ';
        }
        os << '\n';
    }
}
