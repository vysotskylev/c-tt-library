#include "tensor.h"

#include "assertion.h"

#include <iostream>
#include <complex>

template <typename T>
TTensor<T>::TTensor(std::vector<int> modes_sizes_)
    : modes_sizes(std::move(modes_sizes_))
{ }

template <typename T>
TTensor<T>::TTensor(int dimensionality, const int* modes_sizes_)
    : modes_sizes(modes_sizes_, modes_sizes_ + dimensionality)
{ }

template <typename T>
int TTensor<T>::get_dimensionality() const
{
    return modes_sizes.size();
}

template <typename T>
T TTensor<T>::operator[](const int* indices) const
{
    T result;
    ComputeAt(1, indices, &result);
    return result;
}

template <typename T>
T TTensor<T>::operator[](const std::vector<int>& indices) const
{
    T result;
    ComputeAt(1, indices.data(), &result);
    return result;
}

template <typename T>
int TTensor<T>::get_mode_size(int i) const
{
    return i < static_cast<int>(modes_sizes.size()) ? modes_sizes[i] : 0;
}

template <typename T>
std::vector<int> TTensor<T>::get_modes_sizes() const
{
    return modes_sizes;
}

template
class TTensor<double>;
template
class TTensor<float>;
template
class TTensor<std::complex<double>>;
template
class TTensor<std::complex<float>>;

template <typename T>
TUnfoldingSubMatrix<T>::TUnfoldingSubMatrix(
    const TTensor<Value>* tensor,
    int unfolding_index,
    const std::vector<std::vector<int>> &row_indices,
    const std::vector<std::vector<int>> &column_indices)
    : TMatrix<Value>(row_indices.size(), column_indices.size())
    , tensor_(tensor)
    , unfolding_index_(unfolding_index)
    , row_indices_(row_indices)
    , column_indices_(column_indices)
{ }

template <typename T>
void TUnfoldingSubMatrix<T>::ComputeAt(int count, const int* indices, Value* result) const
{
    const int dimensionality = tensor_->get_dimensionality();
    tensor_indices_.clear();
    tensor_indices_.reserve(dimensionality * count);
    for (auto point_idx = 0; point_idx < count; ++point_idx) {
        auto i = indices[point_idx * 2 + 0];
        auto j = indices[point_idx * 2 + 1];
        for (int dim = 0; dim < unfolding_index_; dim++) {
            tensor_indices_.push_back(row_indices_[i][dim]);
        }
        for (int dim = unfolding_index_; dim < dimensionality; dim++) {
            tensor_indices_.push_back(column_indices_[j][dimensionality - dim - 1]);
        }
    }
    tensor_->ComputeAt(count, tensor_indices_.data(), result);
}

template
class TUnfoldingSubMatrix<double>;
template
class TUnfoldingSubMatrix<float>;
template
class TUnfoldingSubMatrix<std::complex<double>>;
template
class TUnfoldingSubMatrix<std::complex<float>>;
