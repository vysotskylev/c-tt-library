#pragma once

#include <cstdio>

#define ENSURE(cond) \
    do { \
        if (!(cond)) { \
            printf("Assertion (%s) at %s:%d failed!\n", # cond, __FILE__, __LINE__); \
            abort(); \
        } \
    } while (false)

#ifdef DEBUG
#define VERIFY(cond) ENSURE(cond)
#else
#define VERIFY(cond)
#endif
