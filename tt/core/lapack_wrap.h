#pragma once

#include "utils.h"

#include <cblas.h>

#include <complex>

template <typename T>
int iamax(int n, const T* x, int incx);

template <typename T>
Underlying<T> nrm2(int n, const T* x, int incx);

template <typename T>
void axpy(int n, T alpha, const T* x, int incx, T* y, int incy);

template <typename T>
void copy(int n, const T* x, int incx, T* y, int incy);

template <typename T>
void scal(int n, T factor, T* x, int incx);

template <typename T>
void gemv(const CBLAS_ORDER order, const CBLAS_TRANSPOSE TransA, const int M, const int N, const T alpha, const T* A, const int lda, const T* X, const int incX, const T beta, T* Y, const int incY);

template <typename T>
void gemm(const CBLAS_ORDER order, const CBLAS_TRANSPOSE TransA, const CBLAS_TRANSPOSE TransB, const int M, const int N, const int K, const T alpha, const T* A, const int lda, const T* B, const int ldb, const T beta, T* C, const int ldc);

template <typename T>
void transpose(int rows, int cols, const T* matrix, T* result);

template <typename T>
T dot_trans(int n, const T* x, int incx, const T* y, int incy);

template <typename T>
T dot_conj(int n, const T* x, int incx, const T* y, int incy);

// A is overwritten.
template <typename T>
void svd(
    int matrix_layout, char jobu, char jobvt,
    int m, int n, T* A,
    int lda, Underlying<T>* S, T* U, int ldu,
    T* VT, int ldvt, Underlying<T>* superb);

// A is overwritten.
template <typename T>
void pseudoinverse(double tolerance, int m, int n, T* A, T* inv_A);

// matrix is replaced with its Q-factor.
template <typename T>
void orthogonalize(int rows, int cols, T* matrix);

// solve system A * X = rhs.
// rhs is replaced with X. A is overwritten.
// A is n x n, rhs is n x nrhs.
template <typename T>
void solve(int n, int nrhs, T* A, T* rhs);

template <typename T>
void compute_singular_numbers(int rows, int cols, T* matrix, Underlying<T>* values);
