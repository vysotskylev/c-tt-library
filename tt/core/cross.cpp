#include "cross.h"
#include "assertion.h"

#include <cmath>
#include <complex>

template <typename T>
TVolume<T>::TVolume(int size, T volume, int* rows, int* columns)
    : volume_(volume)
    , rows_(rows, rows + size)
    , columns_(columns, columns + size)
{ }

template <typename T>
TVolume<T>::TVolume(int size)
    : volume_(0)
    , rows_(size)
    , columns_(size)
{ }

template <typename T>
void TVolume<T>::Set(T volume, int* rows, int* columns)
{
    rows_.assign(rows, rows + rows_.size());
    columns_.assign(columns, columns + columns_.size());
    volume_ = volume;
}

template <typename T>
T TVolume<T>::GetVolume() const
{
    return volume_;
}

template <typename T>
int TVolume<T>::GetRowPosition() const
{
    return rows_[0];
}

template <typename T>
int TVolume<T>::GetColumnPosition() const
{
    return columns_[0];
}

template <typename T>
bool TVolume<T>::operator>(const TVolume &other) const
{
    return std::abs(GetVolume()) > std::abs(other.GetVolume());
}

template <typename T>
bool TVolume<T>::operator==(const TVolume &other) const
{
    return std::abs(volume_) == std::abs(other.GetVolume()) &&
        GetRowPosition() == other.GetRowPosition() &&
        GetColumnPosition() == other.GetColumnPosition();
}

template <typename T>
bool TVolume<T>::operator!=(const TVolume & other) const
{
    return !(*this == other);
}

template
class TVolume<double>;
template
class TVolume<float>;
template
class TVolume<std::complex<double>>;
template
class TVolume<std::complex<float>>;

void TCrossBase::Approximate()
{
    PrepareData();
    SearchMaxVolume();
    while (!StoppingCriterionSatisfied()) {
        UpdateCross();
        SearchMaxVolume();
    }
    ComputeApproximation();
}
