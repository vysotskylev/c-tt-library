#pragma once

#include "cross.h"
#include "lapack_wrap.h"

#include <random>
#include <vector>
#include <limits>
#include <cstdint>

struct TCrossParallelParameters
{
    double tolerance = 0;
    int maximal_iterations_number = 1;
    int number_of_checked_elements = 1;
    int max_rank = 1;
    int rank_increase = 1;
    int stop_rank = 0;
    bool memory_strategy = false;
    bool start_from_column = true;

    // Maximum number of function calls to perform during approximation.
    int64_t evaluation_count_hard_limit = std::numeric_limits<int64_t>::max();
};

template <typename T>
struct TCrossParallelWorkData
{
    TCrossParallelWorkData(
        const TMatrix<T>* original_matrix,
        const TMatrix<T>* approximated_matrix);

    std::vector<bool> IR;
    std::vector<bool> J;
    T *current_row, *current_column;
    TVolume<T> max_volume{1};
    double global_max = 0;
    const TDifferenceMatrix<T> work_matrix;
};

template <typename T>
class TCrossParallel
    : public TCrossBase
    , public TMatrix<T>
{
public:
    using Value = T;

    TCrossParallel(const TMatrix<Value>* matrix, TCrossParallelParameters params);
    ~TCrossParallel();

    void ComputeAt(int count, const int* indices, Value* result) const override;

    int GetRank() const;
    int64_t GetEvaluationCount() const;
    int GetRowPosition(int k) const;
    int GetColumnPosition(int k) const;

    const Value* export_C();
    const Value* export_hat_A_inv();
    const Value* export_RT();
    const Value* export_CAT();
    const Value* export_AR();

private:
    int rank_ = 0;
    int64_t evaluation_count_ = 0;

    const TMatrix<Value>* const matrix_;
    TCrossParallelParameters params_;
    TCrossParallelWorkData<Value> work_data_;

    Value *U = nullptr;
    Value *V = nullptr;
    int* rows_numbers = nullptr;
    int* columns_numbers = nullptr;
    double sq_norm_ = 0;
    std::vector<Value> G_;
    std::vector<Value> RT_;
    std::vector<Value> C_;
    std::mt19937 random_generator_{42};

private:
    void MatrixComputeAt(int count, const int* indices, Value* result);
    void GetDiffRow(int i, Value* result);
    void GetDiffColumn(int j, Value* result);

private:
    void PrepareData() override;
    void SearchMaxVolume() override;
    bool StoppingCriterionSatisfied() const override;
    void UpdateCross() override;
    void ComputeApproximation() override;
};
