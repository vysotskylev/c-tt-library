#include "simple_cross.h"

#include "lapack_wrap.h"
#include "utils.h"
#include "assertion.h"

#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <complex>
#include <exception>
#include <iostream>
#include <fstream>
#include <iterator>

std::ofstream os("/home/levysotsky/Science/tt/fiesta/FIESTA4/cross-parallel-log", std::ios_base::app);

template <typename T>
const T* TCrossParallel<T>::export_C()
{
    return C_.data();
}

template <typename T>
const T* TCrossParallel<T>::export_hat_A_inv()
{
    return G_.data();
}

template <typename T>
const T* TCrossParallel<T>::export_RT()
{
    return RT_.data();
}

template <typename T>
void TCrossParallel<T>::MatrixComputeAt(int count, const int* indices, Value* result)
{
    evaluation_count_ += count;
    matrix_->ComputeAt(count, indices, result);
}

template <typename T>
void TCrossParallel<T>::GetDiffColumn(int j, Value* result)
{
    std::vector<int> indices;
    indices.reserve(this->rows_number);
    for (int i = 0; i < this->rows_number; i++) {
        if (!work_data_.IR[i]) {
            indices.push_back(i);
            indices.push_back(j);
        }
    }
    const int count = indices.size() / 2;
    std::vector<Value> dense_result(count);
    MatrixComputeAt(count, indices.data(), dense_result.data());
    auto dense_result_it = std::begin(dense_result);
    for (int i = 0; i < this->rows_number; i++) {
        if (!work_data_.IR[i]) {
            result[i] = *dense_result_it++;
        }
    }

    copy(this->GetRowCount(), result, 1, C_.data() + rank_ * this->GetRowCount(), 1);
    gemv<Value>(
        CblasColMajor,
        CblasNoTrans,
        this->GetRowCount(),
        rank_,
        -1.0,
        U,
        this->GetRowCount(),
        V + j,
        this->GetColumnCount(),
        1.0,
        result,
        1);
}

template <typename T>
void TCrossParallel<T>::GetDiffRow(int i, Value* result)
{
    std::vector<int> indices;
    indices.reserve(this->columns_number);
    for (int j = 0; j < this->columns_number; j++) {
        if (!work_data_.J[j]) {
            indices.push_back(i);
            indices.push_back(j);
        }
    }
    const int count = indices.size() / 2;
    std::vector<Value> dense_result(count);
    MatrixComputeAt(count, indices.data(), dense_result.data());
    auto dense_result_it = std::begin(dense_result);
    for (int j = 0; j < this->columns_number; j++) {
        if (!work_data_.J[j]) {
            result[j] = *dense_result_it++;
        }
    }

    copy(this->GetColumnCount(), result, 1, RT_.data() + rank_ * this->GetColumnCount(), 1);
    gemv<Value>(
        CblasColMajor,
        CblasNoTrans,
        this->GetColumnCount(),
        rank_,
        -1.0,
        V,
        this->GetColumnCount(),
        U + i,
        this->GetRowCount(),
        1.0,
        result,
        1);
}

template <typename T>
TCrossParallelWorkData<T>::TCrossParallelWorkData(
    const TMatrix<T> *original_matrix,
    const TMatrix<T> *approximated_matrix)
    : IR(original_matrix->GetRowCount())
    , J(original_matrix->GetColumnCount())
    , work_matrix(original_matrix, approximated_matrix)
{ }

template <typename T>
void TCrossParallel<T>::PrepareData()
{
    free(U);
    free(V);
    U = NULL;
    V = NULL;
    free(rows_numbers);
    free(columns_numbers);
    rows_numbers = NULL;
    columns_numbers = NULL;

    if (params_.max_rank <= 0) {
        params_.max_rank = 1;
    }
    if (params_.rank_increase <= 0 && params_.memory_strategy) {
        params_.rank_increase = 1;
    } else if (params_.rank_increase <= 1 && !params_.memory_strategy) {
        params_.rank_increase = 1 + 1;
    }

    U = (T *) calloc(params_.max_rank * this->GetRowCount(), sizeof(T));
    C_.resize(params_.max_rank * this->GetRowCount());
    V = (T *) calloc(params_.max_rank * this->GetColumnCount(), sizeof(T));
    RT_.resize(params_.max_rank * this->GetColumnCount());
    rows_numbers = (int *) realloc(rows_numbers, params_.max_rank*sizeof(int));
    columns_numbers = (int *) realloc(columns_numbers, params_.max_rank*sizeof(int));
}

template <typename T>
TCrossParallel<T>::TCrossParallel(const TMatrix<Value>* matrix, TCrossParallelParameters params)
    : TMatrix<T>(matrix->GetRowCount(), matrix->GetColumnCount())
    , matrix_(matrix)
    , params_(std::move(params))
    , work_data_(matrix, this)
{ }

template <typename T>
TCrossParallel<T>::~TCrossParallel()
{
    free(U);
    free(V);
    free(rows_numbers);
    free(columns_numbers);
}

template <typename T>
int TCrossParallel<T>::GetRank() const
{
    return rank_;
}

template <typename T>
int64_t TCrossParallel<T>::GetEvaluationCount() const
{
    return evaluation_count_;
}

template <typename T>
void TCrossParallel<T>::ComputeAt(int count, const int* indices, Value* result) const
{
    for (auto point_idx = 0; point_idx < count; ++point_idx) {
        auto i = indices[2 * point_idx + 0];
        auto j = indices[2 * point_idx + 1];
        result[point_idx] = dot_trans(rank_, U + i, this->GetRowCount(), V + j, this->GetColumnCount());
    }
}

static int TakeWithoutMask(int i, const std::vector<bool>& mask)
{
    ENSURE(i >= 0);
    for (int result = 0; result < static_cast<int>(mask.size()); ++result) {
        if (!mask[result]) {
            if (i == 0) {
                return result;
            }
            --i;
        }
    }
    ENSURE(false);
}

template <typename T>
void TCrossParallel<T>::SearchMaxVolume()
{
    if (rank_ == std::min(this->GetRowCount(), this->GetColumnCount())) {
        return;
    }

    if (params_.stop_rank > 0 && rank_ == params_.stop_rank) {
        return;
    }

    if (rank_ >= params_.max_rank) {
        if (params_.memory_strategy) {
            params_.max_rank += params_.rank_increase;
        } else {
            params_.max_rank *= params_.rank_increase;
        }
        U = (T *) realloc(U, params_.max_rank * this->GetRowCount() * sizeof(T));
        for (int i = rank_*this->GetRowCount(); i < params_.max_rank * this->GetRowCount(); i++) U[i] = 0.0;
        C_.resize(params_.max_rank * this->GetRowCount());

        V = (T *) realloc(V, params_.max_rank * this->GetColumnCount() * sizeof(T));
        for (int i = rank_*this->GetColumnCount(); i < params_.max_rank * this->GetColumnCount(); i++) V[i] = 0.0;

        RT_.resize(params_.max_rank * this->GetColumnCount());

        rows_numbers = (int *) realloc(rows_numbers, params_.max_rank*sizeof(int));
        columns_numbers = (int *) realloc(columns_numbers, params_.max_rank*sizeof(int));
    }

    work_data_.current_column = U + rank_ * this->GetRowCount();
    work_data_.current_row = V + rank_ * this->GetColumnCount();

    int ik = std::uniform_int_distribution<int>(0, this->GetRowCount() - rank_ - 1)(random_generator_);
    int jk = std::uniform_int_distribution<int>(0, this->GetColumnCount() - rank_ - 1)(random_generator_);

    auto i = TakeWithoutMask(ik, work_data_.IR);
    auto j = TakeWithoutMask(jk, work_data_.J);
    work_data_.max_volume.Set(work_data_.work_matrix.value(i, j), &i, &j);

    auto choose_in_column = [&] {
        auto j = work_data_.max_volume.GetColumnPosition();
        GetDiffColumn(j, work_data_.current_column);
        auto i = work_data_.max_volume.GetRowPosition();
        for (int k = 0, m = this->GetRowCount(); k < m; ++k) {
            if (!work_data_.IR[k] && (std::fabs(work_data_.current_column[i]) < std::fabs(work_data_.current_column[k]))) {
                i = k;
            }
        }
        work_data_.max_volume.Set(work_data_.current_column[i], &i, &j);
    };

    auto choose_in_row = [&] {
        auto i = work_data_.max_volume.GetRowPosition();
        GetDiffRow(i, work_data_.current_row);
        auto j = work_data_.max_volume.GetColumnPosition();
        for (int k = 0, n = this->GetColumnCount(); k < n; ++k) {
            if (!work_data_.J[k]) {
                if (std::fabs(work_data_.current_row[j]) < std::fabs(work_data_.current_row[k])) {
                    j = k;
                }
            }
        }
        work_data_.max_volume.Set(work_data_.current_row[j], &i, &j);
    };

    int prev_column = j;
    int prev_row = i;
    if (params_.start_from_column) {
        for (int iters = 0; iters < params_.maximal_iterations_number; ++iters) {
            auto same_column = (work_data_.max_volume.GetColumnPosition() == prev_column);
            if (!same_column || iters == 0) {
                choose_in_column();
                prev_column = work_data_.max_volume.GetColumnPosition();
            }
            auto same_row = (work_data_.max_volume.GetRowPosition() == prev_row);
            if (!same_row || iters == 0) {
                choose_in_row();
                prev_row = work_data_.max_volume.GetRowPosition();
            }
            if (same_row && iters > 0) {
                break;
            }
        }
    } else {
        for (int iters = 0; iters < params_.maximal_iterations_number; ++iters) {
            auto same_row = (work_data_.max_volume.GetRowPosition() == prev_row);
            if (!same_row || iters == 0) {
                choose_in_row();
                prev_row = work_data_.max_volume.GetRowPosition();
            }
            auto same_column = (work_data_.max_volume.GetColumnPosition() == prev_column);
            if (!same_column || iters == 0) {
                choose_in_column();
                prev_column = work_data_.max_volume.GetColumnPosition();
            }
            if (same_column && iters > 0) {
                break;
            }
        }
    }
    if (prev_column != work_data_.max_volume.GetColumnPosition()) {
        GetDiffColumn(work_data_.max_volume.GetColumnPosition(), work_data_.current_column);
    }
    if (prev_row != work_data_.max_volume.GetRowPosition()) {
        GetDiffRow(work_data_.max_volume.GetRowPosition(), work_data_.current_row);
    }
    if (std::fabs(work_data_.max_volume.GetVolume()) > std::fabs(work_data_.global_max)) {
        work_data_.global_max = std::fabs(work_data_.max_volume.GetVolume());
    }
}

template <typename T>
void TCrossParallel<T>::ComputeApproximation()
{
    U = (T *) realloc(U, rank_ * this->GetRowCount() * sizeof(T));
    V = (T *) realloc(V, rank_ * this->GetColumnCount() * sizeof(T));
    C_.resize(rank_ * this->GetRowCount());
    RT_.resize(rank_ * this->GetColumnCount());
    rows_numbers = (int *) realloc(rows_numbers ,rank_*sizeof(int));
    columns_numbers = (int *) realloc(columns_numbers,rank_*sizeof(int));

    //auto id = rand();
    
    //os << "id_ = " << id << std::endl;
    //os << "rank = " << rank_ << std::endl;

    std::vector<T> hat_A(rank_ * rank_);
    for (int k = 0; k < rank_; k++) {
        copy(rank_, RT_.data() + columns_numbers[k], this->GetColumnCount(), hat_A.data() + k*rank_, 1);
    }
    G_.resize(rank_ * rank_);
    // hat_A is overwritten.
    pseudoinverse(1e-8, rank_, rank_, hat_A.data(), G_.data());

    //WriteMatrix(rank_, rank_, hat_A.data(), "/home/levysotsky/Science/tt/fiesta/FIESTA4/extra/c-tt-library/debug/hat_A_" + std::to_string(id) + ".txt");


    //orthogonalize(this->GetRowCount(), rank_, C_.data());
    //WriteMatrix(this->GetRowCount(), rank_, C_.data(), "/home/levysotsky/Science/tt/fiesta/FIESTA4/extra/c-tt-library/debug/C_" + std::to_string(id) + "_orth.txt");

    //orthogonalize(this->GetColumnCount(), rank_, RT_.data());
    //WriteMatrix(this->GetColumnCount(), rank_, RT_.data(), "/home/levysotsky/Science/tt/fiesta/FIESTA4/extra/c-tt-library/debug/RT_" + std::to_string(id) + "_orth.txt");

    //std::vector<double> hat_C(rank_ * rank_);
    //std::vector<double> hat_RT(rank_ * rank_);
    //for (int k = 0; k < rank_; k++) {
    //    copy(rank_, RT_.data() + columns_numbers[k], this->GetColumnCount(), hat_RT.data() + k, rank_);
    //    copy(rank_, C_.data() + rows_numbers[k], this->GetRowCount(), hat_C.data() + k, rank_);
    //}
    //os << "cols = [";
    //for (int k = 0; k < rank_; k++) {
    //    os << columns_numbers[k] << ", ";
    //}
    //os << "]\nrows = [";
    //for (int k = 0; k < rank_; k++) {
    //    os << rows_numbers[k] << ", ";
    //}
    //os << "]" << std::endl;

    //WriteMatrix(rank_, rank_, hat_RT.data(), "/home/levysotsky/Science/tt/fiesta/FIESTA4/extra/c-tt-library/debug/hat_RT_" + std::to_string(id) + ".txt");
    //WriteMatrix(rank_, rank_, hat_C.data(), "/home/levysotsky/Science/tt/fiesta/FIESTA4/extra/c-tt-library/debug/hat_C_" + std::to_string(id) + ".txt");
    //
    //solve(rank_, rank_, hat_C.data(), hat_A.data());
    //
    //WriteMatrix(rank_, rank_, hat_A.data(), "/home/levysotsky/Science/tt/fiesta/FIESTA4/extra/c-tt-library/debug/sol1_" + std::to_string(id) + ".txt");

    //auto hat_AT = hat_A;
    //transpose(rank_, rank_, hat_A.data(), hat_AT.data());

    //WriteMatrix(rank_, rank_, hat_AT.data(), "/home/levysotsky/Science/tt/fiesta/FIESTA4/extra/c-tt-library/debug/sol1_T_" + std::to_string(id) + ".txt");

    //solve(rank_, rank_, hat_RT.data(), hat_AT.data());

    //WriteMatrix(rank_, rank_, hat_AT.data(), "/home/levysotsky/Science/tt/fiesta/FIESTA4/extra/c-tt-library/debug/sol2_" + std::to_string(id) + ".txt");
   
    //G_ = std::move(hat_A);
    //transpose(rank_, rank_, hat_AT.data(), G_.data());

    //WriteMatrix(rank_, rank_, G_.data(), "/home/levysotsky/Science/tt/fiesta/FIESTA4/extra/c-tt-library/debug/G_" + std::to_string(id) + ".txt");

    //WriteMatrix(this->GetRowCount(), rank_, C_.data(), "/home/levysotsky/Science/tt/fiesta/FIESTA4/extra/c-tt-library/debug/C_" + std::to_string(id) + ".txt");
    //WriteMatrix(this->GetColumnCount(), rank_, RT_.data(), "/home/levysotsky/Science/tt/fiesta/FIESTA4/extra/c-tt-library/debug/RT_" + std::to_string(id) + ".txt");

    //std::vector<double> A(this->GetRowCount() * this->GetColumnCount());
    //for (int i = 0; i < this->GetRowCount(); ++i) {
    //    for (int j = 0; j < this->GetColumnCount(); ++j) {
    //        A[i + j * this->GetRowCount()] = ComputeValue(i, j);
    //    }
    //}
    //WriteMatrix(this->GetRowCount(), this->GetColumnCount(), A.data(), "/home/levysotsky/Science/tt/fiesta/FIESTA4/extra/c-tt-library/debug/A_" + std::to_string(id) + ".txt");

    work_data_.current_row = NULL;
    work_data_.current_column = NULL;
}

template <typename T>
bool TCrossParallel<T>::StoppingCriterionSatisfied() const
{
    auto max_evaluation_count_reached = rank_ >= 1 && evaluation_count_ >= params_.evaluation_count_hard_limit;
    auto max_rank_reached = (rank_ >= std::max(this->GetColumnCount(), this->GetRowCount()));
    auto stop_rank_reached = (params_.stop_rank > 0) && (rank_ >= params_.stop_rank);
    auto adjusted_volume = std::fabs(work_data_.max_volume.GetVolume()) * sqrt(this->GetColumnCount() - rank_) * sqrt(this->GetRowCount() - rank_);
    auto tolerance_reached = (std::sqrt(sq_norm_) * std::fabs(params_.tolerance) >= adjusted_volume);
    return max_evaluation_count_reached || max_rank_reached || stop_rank_reached || tolerance_reached;
}

template <typename T>
void TCrossParallel<T>::UpdateCross()
{
    auto column_factor = T(1.0) / T(sqrt(std::fabs(work_data_.max_volume.GetVolume())));
    auto row_factor = T(1.0) / (work_data_.max_volume.GetVolume() * column_factor);
    for (int i = 0; i < this->GetRowCount(); i++) {
        if (!work_data_.IR[i]) {
            work_data_.current_column[i] *= column_factor;
        } else {
            work_data_.current_column[i] = 0.0;
        }
    }
    for (int j = 0; j < this->GetColumnCount(); j++) {
        if (!work_data_.J[j]) {
            work_data_.current_row[j] *= row_factor;
        } else {
            work_data_.current_row[j] = 0.0;
        }
    }
    work_data_.IR[work_data_.max_volume.GetRowPosition()] = true;
    work_data_.J[work_data_.max_volume.GetColumnPosition()] = true;
    rows_numbers[rank_] = work_data_.max_volume.GetRowPosition();
    columns_numbers[rank_] = work_data_.max_volume.GetColumnPosition();

    for (int i = 0; i < rank_; i++) {
        RT_[rank_ * this->GetColumnCount() + columns_numbers[i]] = this->value(work_data_.max_volume.GetRowPosition(), columns_numbers[i]);
    }
    for (int i = 0; i < rank_; i++) {
        C_[rank_*this->GetRowCount() + rows_numbers[i]] = this->value(rows_numbers[i], work_data_.max_volume.GetColumnPosition());
    }

    auto b = (T *) calloc ((rank_ + 1), sizeof(T));
    auto x = (T *) calloc ((rank_ + 1), sizeof(T));
    gemv<T>(CblasColMajor, CblasConjTrans, this->GetRowCount(), rank_ + 1, 1.0, U, this->GetRowCount(), work_data_.current_column, 1, 0.0, b, 1);
    gemv<T>(CblasColMajor, CblasConjTrans, this->GetColumnCount(), rank_ + 1, 1.0, V, this->GetColumnCount(), work_data_.current_row, 1, 0.0, x, 1);
    sq_norm_ += 2.0 * Real(dot_trans(rank_, b, 1, x, 1)) + Real(b[rank_] * x[rank_]);
    free(x);
    free(b);
    rank_++;
    work_data_.current_column = NULL;
    work_data_.current_row = NULL;
}

template <typename T>
int TCrossParallel<T>::GetRowPosition(int k) const
{
    return rows_numbers[k];
}

template <typename T>
int TCrossParallel<T>::GetColumnPosition(int k) const
{
    return columns_numbers[k];
}

template
class TCrossParallel<float>;
template
class TCrossParallel<double>;
template
class TCrossParallel<std::complex<float>>;
template
class TCrossParallel<std::complex<double>>;
