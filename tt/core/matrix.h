#pragma once

#include <vector>
#include <cstdlib>

template <typename T>
class TMatrix
{
public:
    using Value = T;

    TMatrix(int rows_number, int columns_number);
    virtual ~TMatrix() = default;

    virtual void ComputeAt(int count, const int* indices, Value* result) const = 0;

    std::vector<Value> ComputeAt(int count, const int* indices);
    T value(int i, int j) const;
    int GetRowCount() const;
    int GetColumnCount() const;

protected:
    int rows_number;
    int columns_number;
};

template <typename T>
class TSubMatrix
    : public TMatrix<T>
{
public:
    using Value = typename TMatrix<T>::Value;

    TSubMatrix(int m, int n, int *r, int *c, const TMatrix<Value>& matr);

    void ComputeAt(int count, const int* indices, Value* result) const override;

private:
    std::vector<int> row_indices_;
    std::vector<int> column_indices_;
    const TMatrix<T>& matrix_;
    mutable std::vector<int> original_indices_;
};

template <typename T>
class TDifferenceMatrix
    : public TMatrix<T>
{
public:
    using Value = typename TMatrix<T>::Value;

    TDifferenceMatrix(const TMatrix<Value>* minuend, const TMatrix<Value>* subtrahend);

    void ComputeAt(int count, const int* indices, Value* result) const override;

private:
    const TMatrix<Value>* const minuend_;
    const TMatrix<Value>* const subtrahend_;

    mutable std::vector<Value> subtrahend_values_;
};
