#include "lapack_wrap.h"
#include "assertion.h"

#include <contrib/uvector/uvector.h>

#include <cblas.h>
#include <lapacke.h>

#include <algorithm>
#include <iostream>
#include <vector>

#define CHECK(code) ENSURE(code == 0)

template <>
int iamax(int n, const double* x, int incx)
{
    return cblas_idamax(n, x, incx);
}

template <>
int iamax(int n, const float* x, int incx)
{
    return cblas_isamax(n, x, incx);
}

template <>
double nrm2(int n, const double* x, int incx)
{
    return cblas_dnrm2(n, x, incx);
}

template <>
float nrm2(int n, const float* x, int incx)
{
    return cblas_snrm2(n, x, incx);
}

template <>
double nrm2(int n, const std::complex<double>* x, int incx)
{
    return cblas_dznrm2(n, x, incx);
}

template <>
float nrm2(int n, const std::complex<float>* x, int incx)
{
    return cblas_scnrm2(n, x, incx);
}

template <>
void axpy(int n, double alpha, const double* x, int incx, double* y, int incy)
{
    cblas_daxpy(n, alpha, x, incx, y, incy);
}

template <>
void axpy(int n, float alpha, const float* x, int incx, float* y, int incy)
{
    cblas_saxpy(n, alpha, x, incx, y, incy);
}

template <>
void axpy(int n, std::complex<double> alpha, const std::complex<double>* x, int incx, std::complex<double>* y, int incy)
{
    cblas_zaxpy(n, &alpha, x, incx, y, incy);
}

template <>
void axpy(int n, std::complex<float> alpha, const std::complex<float>* x, int incx, std::complex<float>* y, int incy)
{
    cblas_caxpy(n, &alpha, x, incx, y, incy);
}

template <>
void copy(int n, const double* x, int incx, double* y, int incy)
{
    cblas_dcopy(n, x, incx, y, incy);
}

template <>
void copy(int n, const float* x, int incx, float* y, int incy)
{
    cblas_scopy(n, x, incx, y, incy);
}

template <>
void copy(int n, const std::complex<double>* x, int incx, std::complex<double>* y, int incy)
{
    cblas_zcopy(n, x, incx, y, incy);
}

template <>
void copy(int n, const std::complex<float>* x, int incx, std::complex<float>* y, int incy)
{
    cblas_ccopy(n, x, incx, y, incy);
}

template <>
void scal(int n, double factor, double* x, int incx)
{
   cblas_dscal(n, factor, x, incx);
}

template <>
void scal(int n, float factor, float* x, int incx)
{
   cblas_sscal(n, factor, x, incx);
}

template <>
void scal(int n, std::complex<double> factor, std::complex<double>* x, int incx)
{
   cblas_zscal(n, &factor, x, incx);
}

template <>
void scal(int n, std::complex<float> factor, std::complex<float>* x, int incx)
{
   cblas_cscal(n, &factor, x, incx);
}

template <>
void gemv(const CBLAS_ORDER order, const CBLAS_TRANSPOSE TransA, const int M, const int N, const double alpha, const double* A, const int lda, const double* X, const int incX, const double beta, double* Y, const int incY)
{
    cblas_dgemv(order, TransA, M, N, alpha, A, lda, X, incX, beta, Y, incY);
}

template <>
void gemv(const CBLAS_ORDER order, const CBLAS_TRANSPOSE TransA, const int M, const int N, const float alpha, const float* A, const int lda, const float* X, const int incX, const float beta, float* Y, const int incY)
{
    cblas_sgemv(order, TransA, M, N, alpha, A, lda, X, incX, beta, Y, incY);
}

template <>
void gemv(const CBLAS_ORDER order, const CBLAS_TRANSPOSE TransA, const int M, const int N, const std::complex<double> alpha, const std::complex<double>* A, const int lda, const std::complex<double>* X, const int incX, const std::complex<double> beta, std::complex<double>* Y, const int incY)
{
    cblas_zgemv(order, TransA, M, N, &alpha, A, lda, X, incX, &beta, Y, incY);
}

template <>
void gemv(const CBLAS_ORDER order, const CBLAS_TRANSPOSE TransA, const int M, const int N, const std::complex<float> alpha, const std::complex<float>* A, const int lda, const std::complex<float>* X, const int incX, const std::complex<float> beta, std::complex<float>* Y, const int incY)
{
    cblas_cgemv(order, TransA, M, N, &alpha, A, lda, X, incX, &beta, Y, incY);
}

template <>
void gemm(const CBLAS_ORDER Order, const CBLAS_TRANSPOSE TransA, const CBLAS_TRANSPOSE TransB, const int M, const int N, const int K, const double alpha, const double* A, const int lda, const double* B, const int ldb, const double beta, double* C, const int ldc)
{
    cblas_dgemm(Order, TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc);
}

template <>
void gemm(const CBLAS_ORDER Order, const CBLAS_TRANSPOSE TransA, const CBLAS_TRANSPOSE TransB, const int M, const int N, const int K, const float alpha, const float* A, const int lda, const float* B, const int ldb, const float beta, float* C, const int ldc)
{
    cblas_sgemm(Order, TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc);
}

template <>
void gemm(const CBLAS_ORDER Order, const CBLAS_TRANSPOSE TransA, const CBLAS_TRANSPOSE TransB, const int M, const int N, const int K, const std::complex<double> alpha, const std::complex<double>* A, const int lda, const std::complex<double>* B, const int ldb, const std::complex<double> beta, std::complex<double>* C, const int ldc)
{
    cblas_zgemm(Order, TransA, TransB, M, N, K, &alpha, A, lda, B, ldb, &beta, C, ldc);
}

template <>
void gemm(const CBLAS_ORDER Order, const CBLAS_TRANSPOSE TransA, const CBLAS_TRANSPOSE TransB, const int M, const int N, const int K, const std::complex<float> alpha, const std::complex<float>* A, const int lda, const std::complex<float>* B, const int ldb, const std::complex<float> beta, std::complex<float>* C, const int ldc)
{
    cblas_cgemm(Order, TransA, TransB, M, N, K, &alpha, A, lda, B, ldb, &beta, C, ldc);
}

template <typename T>
void transpose(int rows, int cols, const T* matrix, T* result)
{
    for (int j = 0; j < cols; j++) {
        for (int i = 0; i < rows; ++i) {
            result[i * cols + j] = matrix[j * rows + i];
        }
    }
}

template
void transpose(int rows, int cols, const double* matrix, double* result);
template
void transpose(int rows, int cols, const float* matrix, float* result);
template
void transpose(int rows, int cols, const std::complex<double>* matrix, std::complex<double>* result);
template
void transpose(int rows, int cols, const std::complex<float>* matrix, std::complex<float>* result);

template <>
double dot_trans(int n, const double* x, int incx, const double* y, int incy)
{
   return cblas_ddot(n, x, incx, y, incy);
}

template <>
float dot_trans(int n, const float* x, int incx, const float* y, int incy)
{
   return cblas_sdot(n, x, incx, y, incy);
}

template <>
std::complex<double> dot_trans(int n, const std::complex<double>* x, int incx, const std::complex<double>* y, int incy)
{
    std::complex<double> res;
    cblas_zdotu_sub(n, x, incx, y, incy, &res);
    return res;
}

template <>
std::complex<float> dot_trans(int n, const std::complex<float>* x, int incx, const std::complex<float>* y, int incy)
{
    std::complex<float> res;
    cblas_cdotu_sub(n, x, incx, y, incy, &res);
    return res;
}

template <>
double dot_conj(int n, const double* x, int incx, const double* y, int incy)
{
   return cblas_ddot(n, x, incx, y, incy);
}

template <>
float dot_conj(int n, const float* x, int incx, const float* y, int incy)
{
   return cblas_sdot(n, x, incx, y, incy);
}

template <>
std::complex<double> dot_conj(int n, const std::complex<double>* x, int incx, const std::complex<double>* y, int incy)
{
    std::complex<double> res;
    cblas_zdotc_sub(n, x, incx, y, incy, &res);
    return res;
}

template <>
std::complex<float> dot_conj(int n, const std::complex<float>* x, int incx, const std::complex<float>* y, int incy)
{
    std::complex<float> res;
    cblas_cdotc_sub(n, x, incx, y, incy, &res);
    return res;
}

template <>
void svd(
    int matrix_layout, char jobu, char jobvt,
    int m, int n, double* A,
    int lda, double* S, double* U, int ldu,
    double* VT, int ldvt, double* superb)
{
    CHECK(LAPACKE_dgesvd(matrix_layout, 'S', 'S', m, n, A, m, S, U, m, VT, ldvt, superb));
}

template <>
void svd(
    int matrix_layout, char jobu, char jobvt,
    int m, int n, float* A,
    int lda, float* S, float* U, int ldu,
    float* VT, int ldvt, float* superb)
{
    CHECK(LAPACKE_sgesvd(matrix_layout, 'S', 'S', m, n, A, m, S, U, m, VT, ldvt, superb));
}

template <>
void svd(
    int matrix_layout, char jobu, char jobvt,
    int m, int n, std::complex<double>* A,
    int lda, double* S, std::complex<double>* U, int ldu,
    std::complex<double>* VH, int ldvt, double* superb)
{
    auto A_lapack = reinterpret_cast<lapack_complex_double*>(A);
    auto U_lapack = reinterpret_cast<lapack_complex_double*>(U);
    auto VH_lapack = reinterpret_cast<lapack_complex_double*>(VH);
    CHECK(LAPACKE_zgesvd(matrix_layout, 'S', 'S', m, n, A_lapack, m, S, U_lapack, m, VH_lapack, ldvt, superb));
}

template <>
void svd(
    int matrix_layout, char jobu, char jobvt,
    int m, int n, std::complex<float>* A,
    int lda, float* S, std::complex<float>* U, int ldu,
    std::complex<float>* VH, int ldvt, float* superb)
{
    auto A_lapack = reinterpret_cast<lapack_complex_float*>(A);
    auto U_lapack = reinterpret_cast<lapack_complex_float*>(U);
    auto VH_lapack = reinterpret_cast<lapack_complex_float*>(VH);
    CHECK(LAPACKE_cgesvd(matrix_layout, 'S', 'S', m, n, A_lapack, m, S, U_lapack, m, VH_lapack, ldvt, superb));
}

template <typename T>
void pseudoinverse(double tolerance, int m, int n, T* A, T* inv_A)
{
    auto min_dim = std::min(m ,n);
    ENSURE(min_dim >= 1);

    ao::uvector<T> U(m * min_dim);
    ao::uvector<T> VH(n * min_dim);
    ao::uvector<Underlying<T>> sigma(min_dim);

    ao::uvector<Underlying<T>> superb(min_dim - 1);
    svd(LAPACK_COL_MAJOR, 'S', 'S', m, n, A, m, sigma.data(), U.data(), m, VH.data(), min_dim, superb.data());

    double s = 0;
    for (int i = 0; i < min_dim; i++) {
        s += sigma[i]*sigma[i];
    }
    double r = 0;
    int k = 0;
    for (int i = min_dim; i > 0; i--) {
        r += sigma[i-1]*sigma[i-1];
        if (s*tolerance*tolerance <= r * (1 + tolerance*tolerance)) {
            k = i;
            break;
        }
    }
    for (int i = 0; i < k; i++) {
        scal<T>(n, 1 / sigma[i], &VH[i], min_dim);
    }
    gemm<T>(CblasColMajor, CblasConjTrans, CblasConjTrans, n, m, k, 1, VH.data(), min_dim, U.data(), m, 0, inv_A, n);
}

template
void pseudoinverse(double tolerance, int m, int n, double *A, double *inv_A);
template
void pseudoinverse(double tolerance, int m, int n, float *A, float *inv_A);
template
void pseudoinverse(double tolerance, int m, int n, std::complex<double> *A, std::complex<double> *inv_A);
template
void pseudoinverse(double tolerance, int m, int n, std::complex<float> *A, std::complex<float> *inv_A);

template <>
void orthogonalize(int rows, int cols, double* matrix)
{
    ENSURE(rows >= cols);

    std::vector<double> tau(cols);
    CHECK(LAPACKE_dgeqrf(LAPACK_COL_MAJOR, rows, cols, matrix,  rows, tau.data()));
    CHECK(LAPACKE_dorgqr(LAPACK_COL_MAJOR, rows, cols, cols, matrix, rows, tau.data()));
}

template <>
void orthogonalize(int rows, int cols, float* matrix)
{
    ENSURE(rows >= cols);

    std::vector<float> tau(cols);
    CHECK(LAPACKE_sgeqrf(LAPACK_COL_MAJOR, rows, cols, matrix,  rows, tau.data()));
    CHECK(LAPACKE_sorgqr(LAPACK_COL_MAJOR, rows, cols, cols, matrix, rows, tau.data()));
}

template <>
void orthogonalize(int rows, int cols, std::complex<double>* matrix)
{
    ENSURE(rows >= cols);

    std::vector<std::complex<double>> work(matrix, matrix + rows * cols);
    std::vector<lapack_complex_double> tau(cols);
    auto work_lapack = reinterpret_cast<lapack_complex_double*>(work.data());
    auto matrix_lapack = reinterpret_cast<lapack_complex_double*>(matrix);

    CHECK(LAPACKE_zgeqrf(LAPACK_COL_MAJOR, rows, cols, work_lapack, rows, tau.data()));
    CHECK(LAPACKE_zunmqr(LAPACK_COL_MAJOR, 'L', 'N', rows, cols, cols, work_lapack, rows, tau.data(), matrix_lapack, rows));
}

template <typename>
void orthogonalize(int rows, int cols, std::complex<float>* matrix)
{
    ENSURE(rows >= cols);

    std::vector<std::complex<float>> work(matrix, matrix + rows * cols);
    std::vector<lapack_complex_float> tau(cols);
    auto work_lapack = reinterpret_cast<lapack_complex_float*>(work.data());
    auto matrix_lapack = reinterpret_cast<lapack_complex_float*>(matrix);

    CHECK(LAPACKE_cgeqrf(LAPACK_COL_MAJOR, rows, cols, work_lapack, rows, tau.data()));
    CHECK(LAPACKE_cunmqr(LAPACK_COL_MAJOR, 'L', 'N', rows, cols, cols, work_lapack, rows, tau.data(), matrix_lapack, rows));
}

template <>
void solve(int n, int nrhs, double* A, double* rhs)
{
    std::vector<int> pivots(n);
    CHECK(LAPACKE_dgesv(LAPACK_COL_MAJOR, n, nrhs, A, n, pivots.data(), rhs, n));
}

template <>
void solve(int n, int nrhs, float* A, float* rhs)
{
    std::vector<int> pivots(n);
    CHECK(LAPACKE_sgesv(LAPACK_COL_MAJOR, n, nrhs, A, n, pivots.data(), rhs, n));
}

template <>
void solve(int n, int nrhs, std::complex<double>* A, std::complex<double>* rhs)
{
    std::vector<int> pivots(n);
    auto A_lapack = reinterpret_cast<lapack_complex_double*>(A);
    auto rhs_lapack = reinterpret_cast<lapack_complex_double*>(rhs);
    CHECK(LAPACKE_zgesv(LAPACK_COL_MAJOR, n, nrhs, A_lapack, n, pivots.data(), rhs_lapack, n));
}

template <>
void solve(int n, int nrhs, std::complex<float>* A, std::complex<float>* rhs)
{
    std::vector<int> pivots(n);
    auto A_lapack = reinterpret_cast<lapack_complex_float*>(A);
    auto rhs_lapack = reinterpret_cast<lapack_complex_float*>(rhs);
    CHECK(LAPACKE_cgesv(LAPACK_COL_MAJOR, n, nrhs, A_lapack, n, pivots.data(), rhs_lapack, n));
}

template <>
void compute_singular_numbers(int rows, int cols, double* matrix, double* values)
{
    CHECK(LAPACKE_dgesdd(
        LAPACK_COL_MAJOR,
        'N',
        rows,
        cols,
        matrix,
        rows,
        values,
        0,
        rows,
        0,
        cols
    ));
}

template <>
void compute_singular_numbers(int rows, int cols, float* matrix, float* values)
{
    CHECK(LAPACKE_sgesdd(
        LAPACK_COL_MAJOR,
        'N',
        rows,
        cols,
        matrix,
        rows,
        values,
        0,
        rows,
        0,
        cols
    ));
}

template <>
void compute_singular_numbers(int rows, int cols, std::complex<double>* matrix, double* values)
{
    auto matrix_lapack = reinterpret_cast<lapack_complex_double*>(matrix);
    CHECK(LAPACKE_zgesdd(
        LAPACK_COL_MAJOR,
        'N',
        rows,
        cols,
        matrix_lapack,
        rows,
        values,
        0,
        rows,
        0,
        cols
    ));
}

template <>
void compute_singular_numbers(int rows, int cols, std::complex<float>* matrix, float* values)
{
    auto matrix_lapack = reinterpret_cast<lapack_complex_float*>(matrix);
    CHECK(LAPACKE_cgesdd(
        LAPACK_COL_MAJOR,
        'N',
        rows,
        cols,
        matrix_lapack,
        rows,
        values,
        0,
        rows,
        0,
        cols
    ));
}

#undef CHECK
