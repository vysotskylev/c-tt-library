#pragma once

#include "tensor.h"
#include "utils.h"

#include <contrib/uvector/uvector.h>

#include <limits>
#include <vector>

struct TTensorTrainParameters
{
    double tolerance = 1e-6;
    int maximal_iterations_number = 0;
    int stop_rank = 0;
    int cross_max_iterations = 20;
    int64_t evaluation_count_soft_limit = std::numeric_limits<int64_t>::max();
    int64_t evaluation_count_hard_limit = std::numeric_limits<int64_t>::max();
};

template <typename T>
class TTensorTrain
    : public TTensor<T>
{
public:
    using Value = T;

    struct TApproximationResult
    {
        TTensorTrain approximation;
        double error_estimate;
    };

public:
    void ComputeAt(int count, const int* indices, Value* result) const override;

    Value operator[](int multiindex) const;

    TTensorTrain operator*(Value scalar) const;
    TTensorTrain operator+(const TTensorTrain &other) const;
    TTensorTrain operator-(const TTensorTrain &other) const;

    TTensorTrain() = default;
    TTensorTrain(const TTensorTrain &tensor, Value c);
    TTensorTrain(const TTensorTrain &tensor, const Value *c);

    static TApproximationResult ApproximateWithEstimate(TTensor<Value> *tensor, const TTensorTrainParameters &params);
    static TTensorTrain Approximate(TTensor<Value> *tensor, const TTensorTrainParameters &params);

    static TTensorTrain MakeRankOne(const std::vector<std::vector<Value>>& factors);

    int GetRank(int dim) const;

    double Norm2() const;
    Value Dot(const TTensorTrain &other) const;

    void PrintInfo() const;
    void SaveToFile(const char *file_name) const;
    void LoadFromFile(const char *file_name);

private:
    std::vector<int> ranks;
    std::vector<ao::uvector<Value>> carriages;

private:
    class TApproximator;

private:
    Value ComputeAtPoint(const int* indices) const;
};

template <typename T>
std::vector<std::vector<Underlying<T>>> ComputeUnfoldingMatrixSingularValues(const TTensor<T>& tensor);
