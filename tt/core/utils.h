#pragma once

#include <complex>
#include <string>

void WriteMatrix(int rows, int cols, const double* matrix, const std::string& path);

template <typename T>
struct UnderlyingImpl {
    using Type = T;
};

template <typename T>
struct UnderlyingImpl<std::complex<T>> {
    using Type = T;
};

template <typename T>
using Underlying = typename UnderlyingImpl<T>::Type;

template <typename T>
Underlying<T> Real(T x) {
    return std::complex<Underlying<T>>(x).real();
}
