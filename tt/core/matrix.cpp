#include "matrix.h"
#include "lapack_wrap.h"
#include "assertion.h"

template <typename T>
TMatrix<T>::TMatrix(int m, int n)
    : rows_number(m)
    , columns_number(n)
{ }

template <typename T>
int TMatrix<T>::GetRowCount() const
{
    return rows_number;
}

template <typename T>
int TMatrix<T>::GetColumnCount() const
{
    return columns_number;
}

template <typename T>
std::vector<T> TMatrix<T>::ComputeAt(int count, const int* indices)
{
    std::vector<T> result(count);
    ComputeAt(count, indices, result.data());
    return result;
}

template <typename T>
T TMatrix<T>::value(int i, int j) const
{
    int indices[2] = {i, j};
    T result;
    ComputeAt(1, indices, &result);
    return result;
}

template
class TMatrix<double>;
template
class TMatrix<float>;
template
class TMatrix<std::complex<double>>;
template
class TMatrix<std::complex<float>>;

template <typename T>
TSubMatrix<T>::TSubMatrix(int m, int n, int *r, int *c, const TMatrix<Value>& matr)
    : TMatrix<Value>(m,n)
    , row_indices_(r, r + m)
    , column_indices_(c, c + n)
    , matrix_(matr)
{ }

template <typename T>
void TSubMatrix<T>::ComputeAt(int count, const int* indices, Value* result) const
{
    original_indices_.clear();
    original_indices_.reserve(count * 2);
    for (auto point_idx = 0; point_idx < count; ++point_idx) {
        original_indices_.push_back(row_indices_[point_idx]);
        original_indices_.push_back(column_indices_[point_idx]);
    }
    matrix_.ComputeAt(count, original_indices_.data(), result);
}

template
class TSubMatrix<double>;
template
class TSubMatrix<float>;
template
class TSubMatrix<std::complex<double>>;
template
class TSubMatrix<std::complex<float>>;

template <typename T>
TDifferenceMatrix<T>::TDifferenceMatrix(const TMatrix<Value> *m, const TMatrix<Value> *s)
    : TMatrix<Value>(m->GetRowCount(), m->GetColumnCount())
    , minuend_(m)
    , subtrahend_(s)
{
    ENSURE(m->GetRowCount() == s->GetRowCount() && m->GetColumnCount() == s->GetColumnCount());
}

template <typename T>
void TDifferenceMatrix<T>::ComputeAt(int count, const int* indices, Value* result) const
{
    minuend_->ComputeAt(count, indices, result);
    subtrahend_values_.resize(count);
    subtrahend_->ComputeAt(count, indices, subtrahend_values_.data());
    for (auto point_idx = 0; point_idx < count; ++point_idx) {
        result[point_idx] -= subtrahend_values_[point_idx];
    }
}

template
class TDifferenceMatrix<double>;
template
class TDifferenceMatrix<float>;
template
class TDifferenceMatrix<std::complex<double>>;
template
class TDifferenceMatrix<std::complex<float>>;
